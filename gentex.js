'use strict';

const fs = require('fs');
const path = require('path');

const template = fs.readFileSync('template.tex', 'utf8');
const lines = template.split('\n');

for (const line of lines) {
  if (line.search('%here') >= 0) {
    gentex('src');
  }
  else {
    console.log(line);
  }
}

function gentex(dirname) {
  const SKIP = ['.git', 'tmp', 'Test'];
  const dirs = fs.readdirSync(dirname);
  for (const dir of dirs) {
    if (SKIP.indexOf(dir) >= 0) {
      continue;
    }
    console.log(`\\section{${dir}}`);
    const files = fs.readdirSync(path.join(dirname, dir));
    for (const file of files) {
      if (!['.cpp', '.java'].some(elem => file.endsWith(elem)))
        continue;
      console.log(`\\subsection{${file}}`);
      console.log(`\\lstinputlisting{${path.join(dirname, dir, file)}}`);
    }
    console.log('');
  }
}
