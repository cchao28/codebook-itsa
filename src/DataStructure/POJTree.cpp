/* POJ Tree
 * Given a tree of n nodes, find how many pairs of nodes with distance <= k
 */
#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
#define rep(i, n) for (int i = 0; i < (int)n; ++i)
#define repe(i, a, b) for (int i = a; i <= (int)b; ++i)
#define rii(a, b) scanf("%d%d", &a, &b)
#define riii(a, b, c) scanf("%d%d%d", &a, &b, &c)
#define PB push_back
#define MP make_pair
const int maxn = 10010;
int n, k;
vector<pii> g[maxn];
bool center[maxn] = {};
int ans;
int sz[maxn] = {};
int dfs(int x, int p = 0) {
  sz[x] = 1;
  rep(i, g[x].size()) {
    int y = g[x][i].first;
    if (y == p || center[y])
      continue;
    sz[x] += dfs(y, x);
  }
  return sz[x];
}
pii find_center(int x, int p, int t) {
  pii r = MP(2147483647, -1);
  int s = 1, m = 0;
  rep(i, g[x].size()) {
    int y = g[x][i].first;
    if (y == p || center[y])
      continue;
    r = min(r, find_center(y, x, t));
    m = max(m, sz[y]);
    s += sz[y];
  }
  m = max(m, t - s);
  r = min(r, MP(m, x));
  return r;
}
void enumerate(int x, int p, int d, vector<int> &r) {
  r.PB(d);
  rep(i, g[x].size()) {
    int y = g[x][i].first;
    if (y == p || center[y])
      continue;
    enumerate(y, x, d + g[x][i].second, r);
  }
}
int cal(vector<int> &r) {
  int res = 0;
  sort(r.begin(), r.end());
  int j = r.size();
  rep(i, r.size()) {
    while (j > 0 && r[i] + r[j - 1] > k)
      --j;
    res += j - (j > i ? 1 : 0);
  }
  return res / 2;
}
void solve(int x) {
  dfs(x);
  int y = find_center(x, -1, sz[x]).second;
  center[y] = true;
  rep(i, g[y].size()) if (!center[g[y][i].first]) solve(g[y][i].first);
  vector<int> d;
  d.PB(0);
  rep(i, g[y].size()) if (!center[g[y][i].first]) {
    vector<int> t;
    enumerate(g[y][i].first, y, g[y][i].second, t);
    ans -= cal(t);
    d.insert(d.end(), t.begin(), t.end());
  }
  ans += cal(d);
  center[y] = false;
}
int main() {
  while (rii(n, k) == 2) {
    if (!n && !k)
      break;
    repe(i, 1, n) g[i].clear();
    repe(i, 1, n - 1) {
      int a, b, c;
      riii(a, b, c);
      g[a].push_back(pii(b, c));
      g[b].push_back(pii(a, c));
    }
    ans = 0;
    solve(1);
    printf("%d\n", ans);
  }
  return 0;
}
