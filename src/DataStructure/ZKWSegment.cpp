template<size_t maxn> struct Segment {
  int n, t[maxn * 2];
  void build() {
    for (int i = n - 1; i; --i)
      t[i] = t[i<<1] + t[i<<1|1];
  }
  void modify(int p, int v) {
    for (t[p += n] = v; p > 1; p >>= 1)
      t[p >> 1] = t[p] + t[p ^ 1];
  }
  int query(int l, int r) {
    int sum = 0;
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l&1) sum += t[l++];
      if (r&1) sum += t[--r];
    }
    return sum;
  }
};