struct Treap {
  Treap *l, *r;
  int val, size;
  Treap(int val = 0) : l(NULL), r(NULL), val(val), size(1) {}
  void maintain() { size = (l ? l->size : 0) + (r ? r->size : 0) + 1; }
};
int size(Treap *t) { return t ? t->size : 0; }
Treap *merge(Treap *a, Treap *b) {
  if (!a or !b) return a ? a : b;
  Treap *t = new Treap();
  if (rand() % (a->size + b->size) < a->size) {
    *t = *a;
    t->r = merge(a->r, b);
  } else {
    *t = *b;
    t->l = merge(a, b->l);
  }
  t->maintain();
  return t;
}
void split(Treap *t, int k, Treap *&a, Treap *&b) {
  if (!t) a = b = NULL;
  else if (size(t->l) + 1 <= k) {
    a = new Treap();
    *a = *t;
    split(t->r, k - size(t->l) - 1, a->r, b);
    a->maintain();
  } else {
    b = new Treap();
    *b = *t;
    split(t->l, k, a, b->l);
    b->maintain();
  }
}
