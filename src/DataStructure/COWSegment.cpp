/*
* poj 2104
* input array and query
* output kth number in a[l..r]
*/
#include <algorithm>
#include <cstdio>
using namespace std;
const int maxn = 100000 + 10, maxnode = maxn * 20;
int lc[maxnode], rc[maxnode], sum[maxnode], root[maxnode], sz;
void build(int &x, int l, int r) {
  x = sz++;
  lc[x] = rc[x] = sum[x] = 0;
  if (l == r)
    return;
  int m = (l + r) >> 1;
  build(lc[x], l, m);
  build(rc[x], m + 1, r);
}
void insert(int o, int &n, int l, int r, int x) {
  n = sz++;
  lc[n] = lc[o];
  rc[n] = rc[o];
  sum[n] = sum[o] + 1;
  if (l == r)
    return;
  int m = (l + r) >> 1;
  if (x <= m)
    insert(lc[o], lc[n], l, m, x);
  else
    insert(rc[o], rc[n], m + 1, r, x);
}
int query(int lt, int rt, int l, int r, int x) {
  if (l == r)
    return l;
  int m = (l + r) >> 1, n = sum[lc[rt]] - sum[lc[lt]];
  if (n >= x)
    return query(lc[lt], lc[rt], l, m, x);
  else
    return query(rc[lt], rc[rt], m + 1, r, x - n);
}
int a[maxn], b[maxn], n, q;
int main() {
  while (scanf("%d%d", &n, &q) == 2) {
    for (int i = 0; i < n && scanf("%d", &a[i]); ++i)
      b[i] = a[i];
    sz = 0;
    build(root[0], 1, n);
    sort(b, b + n);
    for (int i = 0; i < n; ++i)
      insert(root[i], root[i + 1], 1, n, lower_bound(b, b + n, a[i]) - b + 1);
    for (int l, r, k; q-- && scanf("%d%d%d", &l, &r, &k);)
      printf("%d\n", b[query(root[l - 1], root[r], 1, n, k) - 1]);
  }
  return 0;
}
