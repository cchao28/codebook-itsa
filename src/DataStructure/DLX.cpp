const int maxnode = 20010, maxc = 2010, maxr = 5010;
#define MT make_tuple
#define FOR(i, A, s) for (int i = A[s]; i != s; i = A[i])
struct DLX {
  int n, sz;
  int S[maxc];
  int row[maxnode], col[maxnode], L[maxnode], R[maxnode], U[maxnode],
      D[maxnode];
  int ansd, ans[maxr];
  void init(int n) {
    this->n = n;
    for (int i = 0; i <= n; ++i) {
      tie(L[i], R[i], U[i], D[i]) = MT(i - 1, i + 1, i, i);
      S[i] = 0;
    }
    R[n] = 0;
    L[0] = n;
    sz = n + 1;
  }
  void addRow(int r, const vector<int> &columns) {
    int fst = sz;
    for (int c : columns) {
      tie(L[sz], R[sz], U[sz], D[sz]) = MT(sz - 1, sz + 1, U[c], c);
      tie(D[U[c]], U[c]) = MT(sz, sz);
      tie(row[sz], col[sz]) = MT(r, c);
      S[c]++;
      sz++;
    }
    R[sz - 1] = fst;
    L[fst] = sz - 1;
  }
  void remove(int c) {
    tie(L[R[c]], R[L[c]]) = MT(L[c], R[c]);
    FOR(i, D, c) FOR(j, R, i) {
      tie(U[D[j]], D[U[j]]) = MT(U[j], D[j]);
      --S[col[j]];
    }
  }
  void restore(int c) {
    tie(L[R[c]], R[L[c]]) = MT(c, c);
    FOR(i, D, c) FOR(j, R, i) {
      tie(U[D[j]], D[U[j]]) = MT(j, j);
      ++S[col[j]];
    }
  }
  bool dfs(int d) {
    if (R[0] == 0) {
      ansd = d;
      return true;
    }
    int c = R[0];
    FOR(i, R, 0) if (S[i] < S[c]) c = i;
    remove(c);
    FOR(i, D, c) {
      ans[d] = row[i];
      FOR(j, R, i) remove(col[j]);
      if (dfs(d + 1))
        return true;
      FOR(j, L, i) restore(col[j]);
    }
    restore(c);
    return false;
  }
  bool solve(vector<int> &v) {
    v.clear();
    if (!dfs(0))
      return false;
    for (int i = 0; i < ansd; ++i)
      v.emplace_back(ans[i]);
    return true;
  }
};
