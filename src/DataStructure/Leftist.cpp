struct Node {
  int v, d;
  Node *l, *r;
  Node(int v) : v(v) {
    d = 0;
    l = r = NULL;
  }
};
inline int dep(Node *x) { return x ? x->d : 0; }
Node *merge(Node *a, Node *b) {
  if (!a || !b)
    return a ? a : b;
  if (a->v < b->v)
    swap(a, b);
  a->r = merge(a->r, b);
  if (dep(a->l) < dep(a->r))
    swap(a->l, a->r);
  a->d = dep(a->r) + 1;
  return a;
}
#include <bits/stdc++.h>
#include <bits/extc++.h>
using namespace std;
#define F first
#define S second
using heap = __gnu_pbds::priority_queue<int>;
// 給定a, n, 求將a改成嚴格非降的代價
int n, a[100010];
pair<heap, int> v[100010];
int sz = 0;
long long solve() {
  for (int i = 0; i < n; ++i) {
    v[sz].F.push(a[i]);
    v[sz].S = 0;
    sz++;
    while (sz >= 2 && v[sz - 2].F.top() > v[sz - 1].F.top()) {
      v[sz - 2].F.join(v[sz - 1].F);
      v[sz - 2].S += v[sz - 1].S;
      sz--;
      while ((int)v[sz - 1].F.size() > v[sz - 1].S + 1) {
        v[sz - 1].F.pop();
        v[sz - 1].S++;
      }
    }
  }
  long long sum = 0;
  for (int i = 0, k = 0; i < sz; ++i) {
    int len = v[i].S + v[i].F.size();
    while (len--) {
      sum += llabs(v[i].F.top() - a[k++]);
    }
  }
  return sum;
}
