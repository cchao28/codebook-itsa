int c[maxn], n;
int sum(int x) {
  int ret = 0;
  for (; x; x -= x & -x)
    ret += c[x];
  return ret;
}
void add(int x, int d) {
  for (; x <= n; x += x & -x)
    c[x] += d;
}
