#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
struct BigInteger : vector<LL> {
  const static int base = 10000, width = 4;
  typedef vector<LL>::const_iterator iter;
  bool negative;
  BigInteger(iter a, iter b) : vector<LL>(a, b) {}
  BigInteger(string s) {
    if (s.empty()) return;
    if (s[0] == '-') { negative = true; s = s.substr(1); }
    else negative = false;
    for (int i = int(s.size()) - 1; i >= 0; i -= width) {
      LL t = 0, b = 1;
      for (int j = i; j > i - width && j >= 0; --j, b *= 10)
        t += (s[j] - '0') * b;
      push_back(t);
    }
    maintain();
  }
  BigInteger(long long x) : BigInteger(to_string(x)) {}
  BigInteger() { negative = false; }
  void maintain() {
    while (size() && back() == 0) pop_back();
    if (empty()) negative = false;
  }
  void carry() {
    for (size_t i = 0; i < size(); ++i) {
      if (at(i) >= 0 && at(i) < base)
        continue;
      if (i + 1u == size())
        push_back(0);
      int r = at(i) % base;
      if (r < 0)
        r += base;
      at(i + 1) += (at(i) - r) / base;
      at(i) = r;
    }
  }
  int abscmp(const BigInteger &rhs) const {
    if (size() > rhs.size()) return 1;
    if (size() < rhs.size()) return -1;
    for (int i = int(size()) - 1; i >= 0; --i) {
      if (at(i) > rhs[i]) return 1;
      if (at(i) < rhs[i]) return -1;
    }
    return 0;
  }
  int cmp(const BigInteger &rhs) const {
    if (negative && !rhs.negative) return -1;
    if (!negative && rhs.negative) return 1;
    int ret = abscmp(rhs);
    if (negative) ret = -ret;
    return ret;
  }
  bool operator<(const BigInteger &rhs) const { return cmp(rhs) == -1; }
  bool operator>(const BigInteger &rhs) const { return cmp(rhs) == 1; }
  bool operator==(const BigInteger &rhs) const { return cmp(rhs) == 0; }
  bool operator!=(const BigInteger &rhs) const { return cmp(rhs) != 0; }
  bool operator<=(const BigInteger &rhs) const { return cmp(rhs) <= 0; }
  bool operator>=(const BigInteger &rhs) const { return cmp(rhs) >= 0; }
  BigInteger abs() const {
    BigInteger r = *this;
    r.negative = false;
    return r;
  }
  BigInteger operator-() const {
    BigInteger r = *this;
    r.negative = !r.negative;
    r.maintain();
    return r;
  }
  BigInteger operator+(const BigInteger &rhs) const {
    if (negative) return -(-(*this) + (-rhs));
    if (rhs.negative) return (*this) - (-rhs);
    BigInteger ret = *this;
    if (rhs.size() > ret.size()) ret.resize(rhs.size());
    for (size_t i = 0; i < rhs.size(); ++i) ret[i] += rhs[i];
    ret.carry();
    ret.maintain();
    return ret;
  }
  BigInteger operator-(const BigInteger &rhs) const {
    if (negative) return -(-(*this) - (-rhs));
    if (rhs.negative) return (*this) + (-rhs);
    if ((*this) < rhs) return -(rhs - (*this));
    BigInteger ret = *this;
    if (rhs.size() > ret.size()) ret.resize(rhs.size());
    for (size_t i = 0; i < rhs.size(); ++i)
      ret[i] -= rhs[i];
    ret.carry();
    ret.maintain();
    return ret;
  }
  BigInteger operator*(const BigInteger &rhs) const {
    BigInteger ret;
    ret.negative = negative != rhs.negative;
    ret.resize(size() + rhs.size());
    for (size_t i = 0; i < size(); ++i)
      for (size_t j = 0; j < rhs.size(); ++j)
        ret[i + j] += at(i) * rhs[j];
    ret.carry();
    ret.maintain();
    return ret;
  }
  BigInteger operator/(const BigInteger &rhs) const {
    BigInteger ret;
    if (size() < rhs.size()) return ret;
    if (negative || rhs.negative) {
      ret = abs() / rhs.abs();
      ret.negative = negative != rhs.negative;
      ret.maintain();
      return ret;
    }
    ret.resize(size() - rhs.size() + 1);
    BigInteger a = *this;
    BigInteger b = rhs;
    a.negative = b.negative = false;
    for (int i = int(ret.size()) - 1; i >= 0; --i) {
      int l = 0, r = base - 1, ans = l;
      while (l <= r) {
        int m = (l + r) >> 1;
        ret[i] = m;
        if ((ret * b) > a) r = m - 1;
        else l = m + 1, ans = m;
      }
      ret[i] = ans;
    }
    ret.carry();
    ret.maintain();
    return ret;
  }
  BigInteger operator%(const BigInteger &rhs) const {
    return (*this) - (*this) / rhs * rhs;
  }
  friend istream &operator>>(istream &i, BigInteger &b) {
    string s;
    i >> s;
    b = BigInteger(s);
    return i;
  }
  friend ostream &operator<<(ostream &o, const BigInteger &b) {
    if (b.size() == 0) o << '0';
    else {
      if (b.negative) o << '-';
      auto it = b.rbegin();
      o << *it++;
      for (; it != b.rend(); ++it) o << setfill('0') << setw(width) << *it;
    }
    return o;
  }
};
BigInteger gcd(const BigInteger &a, const BigInteger &b) {
  if (a % b == BigInteger(0LL)) return b;
  return gcd(b, a % b);
}
int main() {
  BigInteger a, b, ans;
  string op;
  while (cin >> a >> op >> b >> ans) {
    cout << "Read " << a << ' ' << op << ' ' << b << " = " << ans << endl;
    char c = op[0];
    BigInteger tmp;
    if (c == '+') tmp = a + b;
    else if (c == '-') tmp = a - b;
    else if (c == '*') tmp = a * b;
    else if (c == '/') tmp = a / b;
    else if (c == 'g') tmp = gcd(a, b);
    if (tmp != ans) {
      cout << tmp.size() << ' ' << ans.size() << endl;
      cout << tmp.negative << ' ' << ans.negative << endl;
      cout << tmp << ' ' << ans << endl;
    }
    assert(tmp == ans);
    puts("OK");
  }
  return 0;
}
