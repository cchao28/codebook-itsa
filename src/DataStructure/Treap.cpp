struct Treap {
  int key, pri, sz;
  Treap *l, *r;
  Treap(int key = 0) : key(key), pri(rand()), sz(1), l(NULL), r(NULL) {}
};
inline int sz(const Treap *x) { return x ? x->sz : 0; }
inline void mt(Treap *x) {
  if (x) x->sz = sz(x->l) + sz(x->r) + 1;
}
void split(Treap *ori, int k, Treap *&l, Treap *&r) {
  if (ori == NULL || sz(ori) <= k) l = ori, r = NULL;
  else if (k <= 0) l = NULL, r = ori;
  else if (sz(ori->l) >= k) {
    r = ori;
    split(ori->l, k, l, r->l);
  } else {
    l = ori;
    split(ori->r, k - sz(ori->l) - 1, l->r, r);
  }
  mt(l);
  mt(r);
}
void merge(Treap *&res, Treap *l, Treap *r) {
  if (!l || !r) res = !l ? r : l;
  else if (l->pri > r->pri) {
    res = l;
    merge(res->r, l->r, r);
  } else {
    res = r;
    merge(res->l, l, r->l);
  }
  mt(res);
}
