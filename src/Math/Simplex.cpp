#include <bits/stdc++.h>
using namespace std;
const int maxn = 510;
const double eps = 1e-10, inf = 1e100;
#define FOR(i, a, b) for (int i = a; i <= b; ++i)
struct Simplex {
  int n, m;
  double a[maxn][maxn];
  int N[maxn], B[maxn];
  void pivot(int r, int c) {
    swap(N[c], B[r]);
    a[r][c] = 1 / a[r][c];
    FOR(i, 0, n) if (i != c) a[r][i] *= a[r][c];
    FOR(i, 0, m) if (i != r) {
      FOR(j, 0, n) if (j != c) a[i][j] -= a[i][c] * a[r][j];
      a[i][c] = -a[i][c] * a[r][c];
    }
  }
  bool feasible() {
    for (;;) {
      int r, c;
      double p = inf;
      FOR(i, 0, m - 1) if (a[i][n] < p) p = a[r = i][n];
      if (p > -eps)
        return true;
      p = 0;
      FOR(i, 0, n - 1) if (a[r][i] < p) p = a[r][c = i];
      if (p > -eps)
        return false;
      p = a[r][n] / a[r][c];
      FOR(i, r + 1, m - 1) if (a[i][c] > eps) {
        double v = a[i][n] / a[i][c];
        if (v < p) {
          r = i;
          p = v;
        }
      }
      pivot(r, c);
    }
  }
  int solve(int n, int m, double x[maxn], double &ret) {
    this->n = n;
    this->m = m;
    FOR(i, 0, n - 1) N[i] = i;
    FOR(i, 0, m - 1) B[i] = i + n;
    if (!feasible())
      return 0;
    for (;;) {
      int r, c;
      double p = 0;
      FOR(i, 0, n - 1) if (a[m][i] > p) p = a[m][c = i];
      if (p < eps) {
        FOR(i, 0, n - 1) if (N[i] < n) x[N[i]] = 0;
        FOR(i, 0, m - 1) if (B[i] < n) x[B[i]] = a[i][n];
        ret = -a[m][n];
        return 1;
      }
      p = inf;
      FOR(i, 0, m - 1) if (a[i][c] > eps) {
        double v = a[i][n] / a[i][c];
        if (v < p) {
          r = i;
          p = v;
        }
      }
      if (p > inf - eps)
        return -1;
      pivot(r, c);
    }
  }
};
