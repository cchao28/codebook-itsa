bool test(LL n, LL a, LL d) {
  if (a == 0)
    return true;
  while (!(d & 1))
    d >>= 1;
  LL t = pow_mod(a, d, n);
  while ((d != n - 1) && (t != 1) && (t != n - 1)) {
    t = mul_mod(t, t, n);
    d <<= 1;
  }
  return t == n - 1 || (d & 1) == 1;
}
bool isPrime(LL n) {
  if (n < 2)
    return false;
  if (!(n & 1))
    return n == 2;
  // 2, 7, 61 for 2 ** 32
  for (int x : {2, 325, 9375, 28178, 450775, 9780504, 1795265022})
    if (!test(n, x % n, n - 1))
      return false;
  return true;
}
