#include <bits/stdc++.h>
using namespace std;
typedef vector<double> Poly;
typedef vector<Poly> Matrix;
Poly operator*(Poly a, double b) {
  for (auto &x : a) x *= b;
  return a;
}
Poly operator-(Poly a, Poly b) {
  for (size_t i = 0; i < a.size(); ++i)
    a[i] -= b[i];
  return a;
}
Matrix inverse(Matrix A) {
  const int n = A.size();
  Matrix B(n, Poly(n, 0));
  for (int i = 0; i < n; ++i) {
    B[i][i] = 1;
  }
  for (int i = 0; i < n; ++i) {
    for (int j = i; j < n; ++j) {
      if (fabs(A[j][i]) > 0) {
        swap(A[i], A[j]);
        swap(B[i], B[j]);
        break;
      }
    }
    // if (A[i][i] == 0) -> singular
    B[i] = B[i] * (1 / A[i][i]);
    A[i] = A[i] * (1 / A[i][i]);
    for (int j = 0; j < n; ++j)
      if (j != i && fabs(A[j][i]) > 0) {
        B[j] = B[j] - B[i] * A[j][i];
        A[j] = A[j] - A[i] * A[j][i];
      }
  }
  return B;
}
Matrix operator*(Matrix A, Matrix B) {
  const int n = A.size();
  Matrix C(n, Poly(n, 0));
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      for (int k = 0; k < n; ++k)
        C[i][j] += A[i][k] * B[k][j];
  return C;
}
int main() {
  int n;
  while (cin >> n) {
    Matrix A(n, Poly(n, 0));
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        cin >> A[i][j];
    Matrix B = inverse(A);
    Matrix C = A * B;
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        printf("%f%c", B[i][j], " \n"[j == n - 1]);
    puts("");
  }
  return 0;
}

