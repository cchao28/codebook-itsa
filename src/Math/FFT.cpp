const long double PI = acos(0.0) * 2.0;
typedef complex<double> CD;
inline void FFT(vector<CD> &a, bool inverse) {
  const int n = a.size();
  for (int i = 0, j = 0; i < n; i++) {
    if (j > i)
      swap(a[i], a[j]);
    int k = n;
    while (j & (k >>= 1))
      j &= ~k;
    j |= k;
  }
  const double pi = inverse ? -PI : PI;
  for (int step = 1; step < n; step <<= 1) {
    const double alpha = pi / step;
    for (int k = 0; k < step; k++) {
      CD omegak = exp(CD(0, alpha * k));
      for (int Ek = k; Ek < n; Ek += step << 1) {
        int Ok = Ek + step;
        CD t = omegak * a[Ok];
        a[Ok] = a[Ek] - t;
        a[Ek] += t;
      }
    }
  }
  if (inverse)
    for (int i = 0; i < n; i++)
      a[i] /= n;
}
inline vector<double> operator*(const vector<double> &v1,
                                const vector<double> &v2) {
  int s1 = v1.size(), s2 = v2.size(), S = 2;
  while (S < s1 + s2)
    S <<= 1;
  vector<CD> a(S, 0), b(S, 0);
  for (int i = 0; i < s1; i++)
    a[i] = v1[i];
  for (int i = 0; i < s2; i++)
    b[i] = v2[i];
  FFT(a, false);
  FFT(b, false);
  for (int i = 0; i < S; i++)
    a[i] *= b[i];
  FFT(a, true);
  vector<double> res(s1 + s2 - 1);
  for (int i = 0; i < s1 + s2 - 1; i++)
    res[i] = a[i].real();
  return res;
}
