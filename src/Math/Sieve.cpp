#include <bits/stdc++.h>
using namespace std;
const int maxn = 1010;
vector<int> prime;
bool vis[maxn] = {};
int mu[maxn], phi[maxn];
void linear_sieve(int n) {
  mu[1] = phi[1] = 1;
  for (int i = 2; i < n; ++i) {
    if (!vis[i]) {
      prime.push_back(i);
      mu[i] = -1;
      phi[i] = i - 1;
    }
    for (int y : prime) {
      if (1LL * i * y >= n)
        break;
      int t = i * y;
      vis[t] = true;
      if (i % y == 0) {
        mu[t] = 0;
        phi[t] = phi[i] * y;
        break;
      } else {
        mu[t] = -mu[i];
        phi[t] = phi[i] * (y - 1);
      }
    }
  }
}
void getMu(int n) {
  for (int i = 1; i < n; ++i) {
    int target = i == 1 ? 1 : 0;
    int delta = target - mu[i];
    mu[i] = delta;
    for (int j = i + i; j < n; j += i)
      mu[j] += delta;
  }
}
void phi_table(int n) {
  for (int i = 2; i < n; i++)
    phi[i] = 0;
  phi[1] = 1;
  for (int i = 2; i < n; i++)
    if (!phi[i]) for (int j = i; j < n; j += i) {
      if (!phi[j])
        phi[j] = j;
      phi[j] = phi[j] / i * (i - 1);
    }
}
int euler_phi(int n) {
  int m = sqrt(n + 0.5);
  int ans = n;
  for (int i = 2; i <= m; ++i) {
    if (n % i == 0) {
      ans = ans / i * (i - 1);
      while (n % i == 0) n /= i;
    }
  }
  if (n > 1) ans = ans / n * (n - 1);
  return ans;
}
int main() {}
