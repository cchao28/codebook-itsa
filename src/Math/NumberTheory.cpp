const int maxn = 1010;
typedef long long LL;
LL mul_mod(LL a, LL p, const LL m) {
  LL ret = 0;
  for (; p; p >>= 1, a = (a + a) % m)
    if (p & 1)
      ret = (ret + a) % m;
  return ret;
}
LL pow_mod(LL a, LL p, const LL m) {
  LL ret = 1;
  for (; p; p >>= 1, a = mul_mod(a, a, m))
    if (p & 1)
      ret = mul_mod(ret, a, m);
  return ret % m;
}
void ext_gcd(LL a, LL b, LL &d, LL &x, LL &y) {
  if (!b) {
    d = a, x = 1, y = 0;
  } else {
    ext_gcd(b, a % b, d, y, x);
    y -= x * (a / b);
  }
}
int inv_t[maxn];
void inv_table(int n, int mod) {
  inv_t[1] = 1;
  for (int i = 2; i < n; ++i)
    inv_t[i] = (mod - 1LL * (mod / i) * inv_t[mod % i] % mod) % mod;
}

LL fac[maxn], ifac[maxn];
void init_fac(int n, int mod) {
  fac[0] = ifac[0] = 1;
  inv_table(n, mod);
  for (int i = 1; i < n; ++i) {
    fac[i] = fac[i - 1] * i % mod;
    ifac[i] = ifac[i - 1] * inv_t[i] % mod;
    assert(fac[i] * ifac[i] % mod == 1);
  }
}

LL inv(LL a, LL n) {
  LL d, x, y;
  ext_gcd(a, n, d, x, y);
  return d == 1 ? (x % n + n) % n : -1;
}
LL inv(LL a, LL n) {
  return a < 2 ? a : ((1 - n * 1LL * inv(n % a, a)) / a % n + n) % n;
}

// return x, x % m[i] = a[i]
LL CRT(int n, LL *a, LL *m) {
  LL M = 1, d, x = 0, y;
  for (int i = 0; i < n; ++i)
    M *= m[i];
  for (int i = 0; i < n; ++i) {
    LL w = M / m[i];
    ext_gcd(m[i], w, d, d, y);
    x = (x + y * w * a[i]) % M;
  }
  return (x + M) % M;
}

// return x, (a ** x) % n= b
int BabyGiant(int a, int b, int n) {
  int m, v, e = 1;
  m = ceil(sqrt(n + 0.5));
  v = inv(pow_mod(a, m, n), n);
  map<int, int> x;
  x[1] = 0;
  for (int i = 1; i < m; ++i) {
    e = mul_mod(e, a, n);
    if (!x.count(e))
      x[e] = i;
  }
  for (int i = 0; i < m; ++i) {
    if (x.count(b))
      return i * m + x[b];
    b = mul_mod(b, v, n);
  }
  return -1;
}

vector<LL> a;
bool g_test(LL g, LL p) {
  for (LL x : a)
    if (pow_mod(g, (p - 1) / x, p) == 1)
      return false;
  return true;
}
LL primitive_root(LL p) {
  LL tmp = p - 1;
  for (LL i = 2; i <= tmp / i; ++i)
    if (tmp % i == 0) {
      a.push_back(i);
      while (tmp % i == 0)
        tmp /= i;
    }
  if (tmp != 1) a.push_back(tmp);
  for (LL g = 1; ; ++g) {
    if (g_test(g, p)) return g;
  }
}
// POJ 1808: returns x s.t. x ** 2 = a (mod n)
int modsqrt(int a, int n) {
  int b, k, i, x;
  if (n == 2) return a % n;
  if (pow_mod(a, (n - 1) / 2, n) == 1) {
    if (n % 4 == 3) x = pow_mod(a, (n + 1) / 4, n);
    else {
      for (b = 1; pow_mod(b, (n - 1) / 2, n) == 1; ++b);
      i = (n - 1) / 2;
      k = 0;
      do {
        i /= 2;
        k /= 2;
        if ((pow_mod(a, i, n) * (LL)pow_mod(b, k, n) + 1) % n == 0)
          k += (n - 1) / 2;
      } while(i % 2 == 0);
      x = (pow_mod(a, (i + 1) / 2, n) * (LL)pow_mod(b, k / 2, n)) % n;
    }
    if (x * 2 > n)
      x = n - x;
    return x;
  }
  return -1;
}
// Residue: returns all x s.t. x ** n = a (mod p)
vector<int> residue(int p, int n, int a) {
  int g = primitive_root(p);
  LL m = BabyGiant(g, a, p);
  vector<int> ret;
  if (a == 0) {
    ret.push_back(0);
    return ret;
  }
  if (m == -1) return ret;
  LL A = n, B = p - 1, C = m, x, y, d;
  ext_gcd(A, B, d, x, y);
  if (C % d != 0) return ret;
  x = x * (C / d) % B;
  LL delta = B / d;
  for (int i = 0; i < d; ++i) {
    x = ((x + delta) % B + B) % B;
    ret.push_back((int)pow_mod(g, x, p));
  }
  sort(ret.begin(), ret.end());
  ret.erase(unique(ret.begin(), ret.end()), ret.end());
  return ret;
}
// Java Pell: POJ 2427
public static void Pell(int n) {
  BigInteger N, p1, p2, q1, q2, a0, a1, a2, g1, g2, h1, h2,p,q;
  g1 = q2 = p1 = BigInteger.ZERO;
  h1 = q1 = p2 = BigInteger.ONE
  a0 = a1 = BigInteger.valueOf((int)Math.sqrt(1.0*n));
  BigInteger ans=a0.multiply(a0);
  if(ans.equals(BigInteger.valueOf(n))) {
    System.out.println("No solution!");
    return;
  }
  N = BigInteger.valueOf(n)
  while (true) {
    g2 = a1.multiply(h1).subtract(g1);
    h2 = N.subtract(g2.pow(2)).divide(h1);
    a2 = g2.add(a0).divide(h2);
    p = a1.multiply(p2).add(p1);
    q = a1.multiply(q2).add(q1);
    if (p.pow(2).subtract(N.multiply(q.pow(2))).compareTo(BigInteger.ONE) == 0) break;
    g1 = g2;h1 = h2;a1 = a2;
    p1 = p2;p2 = p;
    q1 = q2;q2 = q;
  }
  System.out.println(p+" "+q);
}
