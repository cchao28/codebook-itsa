// Timus 1503
#include <bits/stdc++.h>
using namespace std;
typedef vector<double> Poly;
const double eps = 1e-12;
const double inf = 1e12;
int sign(double x) { return (x > eps) - (x < -eps); }
double get(const Poly &coef, double x) {
  double e = 1, s = 0;
  for (auto c : coef) { s += c * e; e *= x; }
  return s;
}
double find(const Poly &coef, int n, double lo, double hi) {
  double sign_lo, sign_hi;
  if ((sign_lo = sign(get(coef, lo))) == 0) return lo;
  if ((sign_hi = sign(get(coef, hi))) == 0) return hi;
  if (sign_lo * sign_hi > 0) return inf;
  for (int step = 0; step < 100 && hi - lo > eps; ++ step) {
    double m = (lo + hi) * 0.5;
    int sign_mid = sign(get(coef, m));
    if (sign_mid == 0) return m;
    if (sign_lo * sign_mid < 0) { hi = m; } else { lo = m; }
  }
  return (lo + hi) * 0.5;
}
vector<double> equation(Poly coef, int n) {
  vector<double> ret;
  if (n == 1) {
    if (sign(coef[1])) ret.push_back(-coef[0] / coef[1]);
    return ret;
  }
  vector<double> dcoef(n);
  for (int i = 0; i < n; ++i) dcoef[i] = coef[i + 1] * (i + 1);
  vector<double> droot = equation(dcoef, n - 1);
  droot.insert(droot.begin(), -inf);
  droot.push_back(inf);
  for (int i = 0; i + 1 < droot.size(); ++i) {
    double tmp = find(coef, n, droot[i], droot[i + 1]);
    if (tmp < inf) ret.push_back(tmp);
  }
  return ret;
}
int main() {
  int n; scanf("%d", &n); vector<double> a(n + 1);
  for (int i = n; i >= 0; --i) scanf("%lf", &a[i]);
  auto ans = equation(a, n);
  sort(ans.begin(), ans.end());
  for (auto x : ans) printf("%.12f\n", x);
  return 0;
}

