// given a, m, m != 0
// find s, t, i.e. a ** (phi(t) + s) == a ** s
pii ExtEuler(int a, int m) {
  for (int i = 0;; ++i) {
    int d = __gcd(a, m);
    int mm = m / d;
    if (d == 1) return {i, m};
    a = d; m = mm;
  }
}
int main() {
  int a, m;
  while (scanf("%d%d", &a, &m) == 2) {
    pii ans = ExtEuler(a, m);
    printf("%d %d\n", ans.first, ans.second);
  }
  return 0;
}
