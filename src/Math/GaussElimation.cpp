#include <bits/stdc++.h>
using namespace std;
const int maxn = 110;
const double eps = 1e-8;
int gauss(double a[maxn][maxn], int n) {
  int rank = 0;
  for (int i = 0; i < n; ++i) {
    int r = i;
    for (int j = i + 1; j < n; ++j)
      if (fabs(a[j][i]) > fabs(a[r][i]))
        r = j;
    if (fabs(a[r][i]) < eps)
      continue;
    else
      rank++;
    if (r != i)
      for (int j = 0; j <= n; ++j)
        swap(a[r][j], a[i][j]);
    for (int k = 0; k < n; ++k)
      if (k != i) {
        double tmp = a[k][i] / a[i][i];
        for (int j = i; j <= n; ++j)
          a[k][j] -= a[i][j] * tmp;
      }
  }
  return rank;
}
int main() {}
