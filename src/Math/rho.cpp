#include <bits/stdc++.h>
using namespace std;
long long f(long long x) { return x * x + 1; }
long long rho(long long n) {
  long long x = 2, y = 2, d = 1;
  while (d == 1) {
    x = f(x) % n;
    if (x >= n)
      x -= n;
    y = f(f(y)) % n;
    if (y >= n)
      y -= n;
    d = __gcd(llabs(x - y), n);
  }
  return d;
}
int main() {
  long long n;
  while (cin >> n)
    while (n > 1) {
      long long x = rho(n);
      cout << "factor " << x << endl;
      n /= x;
    }
  return 0;
}
