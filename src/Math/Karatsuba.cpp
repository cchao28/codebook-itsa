// Karatsuba with mod, for Codeforces 100341C
LL buf[10000000], *ptr = buf;
LL *alloc(size_t n) {
  LL *ret = ptr;
  memset(ret, 0, sizeof(LL) * n);
  ptr += n;
  return ret;
}
void mul(int n, LL *a, LL *b, LL *c) {
  if (n < 32) {
    REP(i, n) REP(j, n)
      c[i + j] += a[i] * b[j];
    REP(i, n * 2 - 1) c[i] %= mod;
    return ;
  }
  int m = n >> 1, o = n - m;
  auto op = ptr;
  auto x = alloc(m * 2 - 1), y = alloc(o * 2 - 1), z = alloc(o * 2 - 1);
  auto aa = alloc(o), bb = alloc(o);
  REP(i, n) {
    int idx = i < m ? i : i - m;
    aa[idx] += a[i];
    bb[idx] += b[i];
  }
  mul(m, a, b, x);
  mul(o, a + m, b + m, y);
  mul(o, aa, bb, z);
  memcpy(c, x, sizeof(LL) * (m * 2 - 1));
  memcpy(c + m + m, y, sizeof(LL) * (o * 2 - 1));
  REP(i, o * 2 - 1) c[i + m] += z[i] - x[i] - y[i];
  REP(i, n * 2 - 1) { c[i] %= mod; if (c[i] < 0) c[i] += mod; }
  ptr = op;
}
