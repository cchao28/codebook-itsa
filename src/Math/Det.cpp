typedef vector<LL> Poly;
typedef vector<Poly> Matrix;
const LL mod = 100000007;
Poly operator*(Poly a, LL b) {
  for (auto &x : a) x = (x * b) % mod;
  return a;
}
Poly operator-(Poly a, Poly b) {
  for (size_t i = 0; i < a.size(); ++i) {
    a[i] -= b[i];
    if (a[i] < 0) a[i] += mod;
  }
  return a;
}
LL Det(Matrix A) {
  const int n = A.size();
  LL ret = 1;
  for (int i = 0; i < n; ++i) {
    for (int j = i; j < n; ++j) {
      if (llabs(A[j][i]) > 0) {
        swap(A[i], A[j]);
        if (j != i)
          ret = mod - ret;
        break;
      }
    }
    if (A[i][i] == 0)
      return 0;
    ret = ret * A[i][i] % mod;
    LL iv = inv(A[i][i], mod);
    A[i] = A[i] * iv;
    for (int j = 0; j < n; ++j)
      if (j != i && llabs(A[j][i]) > 0) {
        A[j] = A[j] - A[i] * A[j][i];
      }
  }
  return ret;
}
