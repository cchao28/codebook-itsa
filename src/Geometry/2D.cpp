#include <bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
typedef complex<double> cd;
const double eps = 1e-8;
int dcmp(double x) { return (x > eps) - (x < -eps); }
namespace std {
bool operator<(const cd &a, const cd &b) {
  int x = dcmp(a.real() - b.real());
  int y = dcmp(a.imag() - b.imag());
  return pii(x, y) < pii(0, 0);
}
bool operator==(const cd &a, const cd &b) {
  int x = dcmp(a.real() - b.real());
  int y = dcmp(a.imag() - b.imag());
  return pii(x, y) == pii(0, 0);
}
}
double dot(cd a, cd b) { return real(conj(a) * b); }
double cross(cd a, cd b) { return imag(conj(a) * b); }
cd rotate(cd a, double rad) { return a * exp(cd(0, rad)); }
cd intersection(cd p, cd v, cd q, cd w) {
  return p + v * (cross(w, p - q) / cross(v, w));
}
double distance_line(cd p, cd a, cd b) {
  return fabs(cross(b - a, p - a)) / abs(b - a);
}
double distance_seg(cd p, cd a, cd b) {
  if (a == b)
    return abs(p - a);
  cd v1 = b - a, v2 = p - a, v3 = p - b;
  if (dot(v1, v2) < 0)
    return abs(v2);
  else if (dot(v1, v3) > 0)
    return abs(v3);
  else
    return distance_line(p, a, b);
}
cd Normal(cd A) {
  double L = abs(A);
  return cd(-A.imag() / L, A.real() / L);
}
bool SegmentProperIntersection(const cd &a1, const cd &a2, const cd &b1,
                               const cd &b2) {
  double c1 = cross(a2 - a1, b1 - a1), c2 = cross(a2 - a1, b2 - a1),
         c3 = cross(b2 - b1, a1 - b1), c4 = cross(b2 - b1, a2 - b1);
  return dcmp(c1) * dcmp(c2) < 0 && dcmp(c3) * dcmp(c4) < 0;
}
cd centroid(cd *p, int n) {
  double scs = 0;
  double cs = cross(p[n - 1], p[0]);
  double rx = (p[n - 1].real() + p[0].real()) * cs;
  double ry = (p[n - 1].imag() + p[0].imag()) * cs;
  scs += cs;
  for (int i = 0; i < n - 1; ++i) {
    cs = cross(p[i], p[i + 1]);
    rx += (p[i].real() + p[i + 1].real()) * cs;
    ry += (p[i].imag() + p[i + 1].imag()) * cs;
    scs += cs;
  }
  scs = scs * 3;
  return cd(rx / scs, ry / scs);
}
cd projection(cd p, cd a, cd b) {
  cd v = b - a;
  return a + v * (dot(v, p - a) / dot(v, v));
}
vector<cd> ConvexHull(vector<cd> p) {
  sort(p.begin(), p.end());
  p.erase(unique(p.begin(), p.end()), p.end());
  int n = p.size(), m = 0;
  vector<cd> ch(n + 1);
  for (int i = 0; i < n; ++i) {
    while (m > 1 && cross(ch[m - 1] - ch[m - 2], p[i] - ch[m - 2]) <= 0)
      m--;
    ch[m++] = p[i];
  }
  int k = m;
  for (int i = n - 2; i >= 0; --i) {
    while (m > k && cross(ch[m - 1] - ch[m - 2], p[i] - ch[m - 2]) <= 0)
      m--;
    ch[m++] = p[i];
  }
  if (n > 1)
    m--;
  ch.resize(m);
  return ch;
}
double area(vector<cd> p) {
  int n = p.size();
  double area = 0;
  for (int i = 1; i < n - 1; ++i)
    area += cross(p[i] - p[0], p[i + 1] - p[0]);
  return fabs(area / 2);
}
struct Line {
  cd p;
  cd v;
  double ag;
  Line(){};
  Line(cd p, cd v) : p(p), v(v) { ag = arg(v); }
};
bool onLeft(Line l, cd p) { return cross(l.v, p - l.p) > 0; }
cd intersection(Line a, Line b) { return intersection(a.p, a.v, b.p, b.v); }
vector<cd> HalfPlane(vector<Line> l) {
  sort(l.begin(), l.end(), [&](Line a, Line b) { return a.ag < b.ag; });
  vector<cd> p(l.size());
  vector<Line> q(l.size());
  vector<cd> ret;
  int front, rear;
  q[front = rear = 0] = l[0];
  for (unsigned i = 1; i < l.size(); ++i) {
    while (front < rear && !onLeft(l[i], p[rear - 1]))
      rear--;
    while (front < rear && !onLeft(l[i], p[front]))
      front++;
    q[++rear] = l[i];
    if (fabs(cross(q[rear].v, q[rear - 1].v)) < eps) {
      rear--;
      if (onLeft(q[rear], l[i].p))
        q[rear] = l[i];
    }
    if (front < rear)
      p[rear - 1] = intersection(q[rear - 1], q[rear]);
  }
  while (front < rear && !onLeft(q[front], p[rear - 1]))
    rear--;
  if (rear - front <= 1)
    return ret;
  p[rear] = intersection(q[rear], q[front]);
  for (int i = front; i <= rear; ++i)
    ret.push_back(p[i]);
  return ret;
}
bool OnSegment(cd p, cd a1, cd a2) {
  return dcmp(cross(a1 - p, a2 - p)) == 0 && dcmp(dot(a1 - p, a2 - p)) < 0;
}
int PointInPolygon(const cd p, const vector<cd> &poly) {
  int wn = 0, n = poly.size();
  for (int i = 0; i < n; ++i) {
    const cd &p1 = poly[i], &p2 = poly[(i + 1) % n];
    if (p1 == p || p2 == p || OnSegment(p, p1, p2))
      return -1; // border
    int k = dcmp(cross(p2 - p, p - p1));
    int d1 = dcmp(p1.imag() - p.imag());
    int d2 = dcmp(p2.imag() - p.imag());
    if (k > 0 && d1 <= 0 && d2 > 0)
      wn++;
    if (k < 0 && d2 <= 0 && d1 > 0)
      wn--;
  }
  if (wn != 0)
    return 1; // in
  return 0;   // out
}

// Circle Circle Intersection
cd rotate(const cd &p, double cost, double sint) {
  double x = p.real(), y = p.imag();
  return cd(x * cost - y * sint, x * sint + y * cost);
}
pair<cd, cd> crosspoint(cd ap, double ar, cd bp, double br) {
  double d = abs(ap - bp);
  double cost = (ar * ar + d * d - br * br) / (2 * ar * d);
  double sint = sqrt(1 - cost * cost);
  cd v = (bp - ap) / abs(bp - ap) * ar;
  return {ap + rotate(v, cost, -sint), ap + rotate(v, cost, sint)};
}
// Circle Union Area
// @param m: number of circles.
// @param tc: circles to calculate.
// Verify: SPOJ CIRCU
struct Circle {
  cd p;
  double r;
  bool operator<(const Circle &o) const {
    if (dcmp(r - o.r) != 0) return dcmp(r - o.r) == -1;
    return p < o.p;
  }
  bool operator==(const Circle &o) const {
    return dcmp(r - o.r) == 0 && p == o.p;
  }
} c[1010], tc[1010];
pair<cd, cd> crosspoint(const Circle &a, const Circle &b) {
  return crosspoint(a.p, a.r, b.p, b.r);
}
int n, m;
struct Node {
  cd p; double a; int d;
  bool operator < (const Node &o) const { return a < o.a; }
};
double solve() {
  sort(tc, tc + m);
  m = unique(tc, tc + m) - tc;
  for (int i = m - 1; i >= 0; --i) {
    bool ok = true;
    for (int j = i + 1; j < m; ++j) {
      double d = abs(tc[i].p - tc[j].p);
      if (dcmp(d - abs(tc[i].r - tc[j].r)) <= 0) {
        ok = false;
        break;
      }
    }
    if (ok) c[n++] = tc[i];
  }
  double ans = 0;
  for (int i = 0; i < n; ++i) {
    vector<Node> event;
    cd boundary = c[i].p + cd(-c[i].r, 0);
    event.push_back({boundary, -M_PI, 0});
    event.push_back({boundary, M_PI, 0});
    for (int j = 0; j < n; ++j) {
      if (i == j) continue;
      double d = abs(c[i].p - c[j].p);
      if (dcmp(d - (c[i].r + c[j].r)) < 0) {
        pair<cd, cd> ret = crosspoint(c[i], c[j]);
        double x = arg(ret.first - c[i].p);
        double y = arg(ret.second - c[i].p);
        if (dcmp(x - y) > 0) {
          event.push_back({ret.first, x, 1});
          event.push_back({boundary, M_PI, -1});
          event.push_back({boundary, -M_PI, 1});
          event.push_back({ret.second, y, -1});
        } else {
          event.push_back({ret.first, x, 1});
          event.push_back({ret.second, y, -1});
        }
      }
    }
    sort(event.begin(), event.end());
    int sum = event[0].d;
    for (int j = 1; j < int(event.size()); ++j) {
      if (sum == 0) {
        ans += cross(event[j - 1].p, event[j].p) / 2;
        double x = event[j - 1].a;
        double y = event[j].a;
        double area = c[i].r * c[i].r * (y - x) / 2;
        cd v1 = event[j - 1].p - c[i].p;
        cd v2 = event[j].p - c[i].p;
        area -= cross(v1, v2) / 2;
        ans += area;
      }
      sum += event[j].d;
    }
  }
  return ans;
}
// Four centers of triangles
cd mass_center(cd a, cd b, cd c) { return (a + b + c) / 3.0; }
cd circum_center(cd a, cd b, cd c) {
  double a1 = real(b - a), b1 = imag(b - a), c1 = (a1 * a1 + b1 * b1) / 2.0;
  double a2 = real(c - a), b2 = imag(c - a), c2 = (a2 * a2 + b2 * b2) / 2.0;
  double d = a1 * b2 - a2 * b1;
  return a + cd(c1 * b2 - c2 * b1, a1 * c2 - a2 * c1) / d;
}
cd ortho_center(cd a, cd b, cd c) {
  return mass_center(a, b, c) * 3.0 - circum_center(a, b, c) * 2.0;
}
cd inner_center(cd a, cd b, cd c) {
  double la = abs(b - c);
  double lb = abs(c - a);
  double lc = abs(a - b);
  return (a * la + b * lb + c * lc) / (la + lb + lc);
}
int main() {}
