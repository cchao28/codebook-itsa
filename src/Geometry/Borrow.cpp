/**
 * 求凸多边形直径！注意传入凸多边形！
 */
double diam(Point *p, int n) {
  if(area(p, n)<0)  reverse(p, p+n);
  p[n] = p[0];
  double res = 0;
  for(int i = 0, j = 1; i < n; i ++) {
    while(sig(cross(p[i], p[i+1], p[j])-cross(p[i], p[i+1], p[(j+1)%n])) < 0)
      j = (j+1)%n;
    res = max(res, dis(p[i], p[j]));
    res = max(res, dis(p[i+1], p[j]));
  }
  return res;
}
//计算两个凸多边形间最近，最远距离，assume两个凸多边形分离
//返回o到线段ab的最短距离
double minDis(Point o, Point a, Point b) {
  if(sig(dot(b, a, o))<0) return dis(o, b);
  if(sig(dot(a, b, o))<0) return dis(o, a);
  return fabs(cross(o, a, b)/dis(a, b));
}
//计算从curAng逆时针转到ab的角
double calRotate(Point a, Point b, double curAng) {
  double x = fmod(atan2(b.y-a.y, b.x-a.x)-curAng, M_PI*2);
  if(x<0)     x += M_PI*2;
  if(x>1.9*M_PI)  x = 0;    //in case x is nearly 2*M_PI
  if(x >= 1.01*M_PI)  while(1);
  return x;
}
//求凸多边形间最小距离，断言P和Q分离
//传入P、Q：凸多边形。n、m：P和Q的顶点个数
//如果P和Q非逆时针，则reverse!
//题目：POJ-3608
//教程：http://blog.csdn.net/ACMaker/archive/2008/10/29/3178696.aspx
double mind2p(Point *P, int n, Point *Q, int m) {
  if(area(P, n) < 0)  reverse(P, P+n);  //需要逆时针的
  if(area(Q, m) < 0)  reverse(Q, Q+m);
  int p0 = min_element(P, P+n)-P, q0 = max_element(Q, Q+m)-Q;
  double res = dis(P[p0], Q[q0]);
  double ang = -M_PI/2, rotateP, rotateQ;
  int pi, pj, qi, qj, totP, totQ, val;
  for(pi=p0, qi=q0, totP=0, totQ=0; totP<n && totQ<m; ) {
    pj = (pi+1) % n;
    qj = (qi+1) % m;
    rotateP = calRotate(P[pi], P[pj], ang);
    rotateQ = calRotate(Q[qi], Q[qj], ang+M_PI);
    val = sig(rotateP - rotateQ);
    ang += min(rotateP, rotateQ);
    if(val == -1) res = min(res, minDis(Q[qi], P[pi], P[pj]));
    else if(val==1) res = min(res, minDis(P[pi], Q[qi], Q[qj]));
    else {
      res = min(res, minDis(P[pi], Q[qi], Q[qj]));
      res = min(res, minDis(P[pj], Q[qi], Q[qj]));
      res = min(res, minDis(Q[qi], P[pi], P[pj]));
      res = min(res, minDis(Q[qj], P[pi], P[pj]));
    }
    if(val != 1)  pi=pj, totP ++;
    if(val != -1) qi=qj, totQ ++;
  }
  return res;
}
//求凸多边形间最大距离，断言P和Q分离
//传入P、Q：凸多边形。n、m：P和Q的顶点个数
//如果P和Q非逆时针，则reverse!
//教程：http://blog.csdn.net/ACMaker/archive/2008/10/29/3178794.aspx
double maxd2p(Point *P, int n, Point *Q, int m) { //【【【待测】】】.......!!!
  if(area(P, n) < 0)  reverse(P, P+n);  //需要逆时针的
  if(area(Q, m) < 0)  reverse(Q, Q+m);
  int p0 = min_element(P, P+n)-P, q0 = max_element(Q, Q+m)-Q;
  double res = dis(P[p0], Q[q0]);
  double ang = -M_PI/2, rotateP, rotateQ;
  int pi, pj, qi, qj, totP, totQ, val;
  for(pi=p0, qi=q0, totP=0, totQ=0; totP<n && totQ<m; ) {
    pj = (pi+1) % n;
    qj = (qi+1) % m;
    rotateP = calRotate(P[pi], P[pj], ang);
    rotateQ = calRotate(Q[qi], Q[qj], ang+M_PI);
    val = sig(rotateP - rotateQ);
    ang += min(rotateP, rotateQ);
    if(val == -1) res = max(res, max(dis(Q[qi], P[pi]), dis(Q[qi], P[pj])));
    else if(val==1) res = max(res, max(dis(P[pi], Q[qi]), dis(P[pi], Q[qj])));
    else {
      res = max(res, dis(P[pi], Q[qi]));
      res = max(res, dis(P[pi], Q[qj]));
      res = max(res, dis(P[pj], Q[qi]));
      res = max(res, dis(P[pj], Q[qj]));
    }
    if(val != 1)  pi=pj, totP ++;
    if(val != -1) qi=qj, totQ ++;
  }
  return res;
}
/**
 * 计算最多共线的点的个数(必须是整点，不能有重点，效率n^2),不能有重点!!
 */
typedef pair<int,int> T;
int gcd(int a, int b) {
  for(int t; t=b; b=a%b, a=t);
  return a;
}
int calNumLine(T *ts, int n) {
  static T tmp[maxn];
  int res = 0;
  int i, j, k;
  for(i = 0; i < n; i ++) {
    for(j = 0; j < i; j ++) {
      int dx = ts[j].first - ts[i].first;
      int dy = ts[j].second-ts[i].second;
      int g = gcd(dx, dy);
      dx /= g;  dy /= g;
      if(dx<0)  dx=-dx,dy=-dy;
      tmp[j] = T(dx, dy);
    }
    sort(tmp, tmp+j);
    for(j = 0; j < i; j = k) {
      for(k = j; k<i && tmp[k]==tmp[j]; k ++);
      res = max(res, k-j);
    }
  }
  return res+1;
}
