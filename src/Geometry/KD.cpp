// UVa 1683 In case of failure
#include <bits/stdc++.h>
using namespace std;
struct Point { long long a[2]; int no; };
int cno;
bool cmp(Point a, Point b) { return a.a[cno] < b.a[cno]; }
void build(int l, int r, Point *p, int d) {
  if (l > r)
    return;
  int mid = (l + r) >> 1;
  cno = d;
  nth_element(p + l, p + mid, p + r + 1, cmp);
  build(l, mid - 1, p, d ^ 1);
  build(mid + 1, r, p, d ^ 1);
}
inline long long sq(long long x) { return x * x; }
inline long long dis2(Point a, Point b) {
  return sq(a.a[0] - b.a[0]) + sq(a.a[1] - b.a[1]);
}
long long mi;
int no;
void query(int l, int r, Point *p, int d, Point q) {
  if (l > r)
    return;
  int mid = (l + r) >> 1;
  if (p[mid].no != q.no) {
    long long t = dis2(p[mid], q);
    if (t < mi || (t == mi && p[mid].no < no)) {
      mi = t;
      no = p[mid].no;
    }
  }
  long long t = q.a[d] - p[mid].a[d];
  if (t <= 0) {
    query(l, mid - 1, p, d ^ 1, q);
    if (mi >= t * t)
      query(mid + 1, r, p, d ^ 1, q);
  } else {
    query(mid + 1, r, p, d ^ 1, q);
    if (mi >= t * t)
      query(l, mid - 1, p, d ^ 1, q);
  }
}
int n;
Point p[100010];
long long ans[100010];
int main() {
  int T;
  scanf("%d", &T);
  while (T--) {
    scanf("%d", &n);
    for (int i = 0, a, b; i < n; ++i) {
      scanf("%d%d", &a, &b);
      p[i].a[0] = a, p[i].a[1] = b, p[i].no = i + 1;
    }
    build(0, n - 1, p, 0);
    for (int i = 0; i < n; ++i) {
      mi = 1000000000LL * 1000000000LL + 100;
      no = 0;
      query(0, n - 1, p, 0, p[i]);
      ans[p[i].no] = mi;
    }
    for (int i = 1; i <= n; ++i)
      printf("%lld\n", ans[i]);
  }
  return 0;
}
