typedef pair<cd, cd> Seg; Seg seg[1010]; int n;
vector<cd> p; vector<int> e, nxt, head; vector<bool> mark;
void add(int a, int b) {
  e.push_back(b);
  nxt.push_back(head[a]);
  head[a] = (e.size() - 1);
  e.push_back(a);
  nxt.push_back(head[b]);
  head[b] = (e.size() - 1);
}
int main() {
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    int a, b, c, d;
    scanf("%d%d%d%d", &a, &b, &c, &d);
    if (a == c && b == d) { n--; i--; continue; }
    seg[i] = {cd(a, b), cd(c, d)};
    seg[i].S -= seg[i].F;
  }
  vector<vector<int>> line(n);
  for (int i = 0; i < n; ++i)
    for (int j = i + 1; j < n; ++j) {
      if (dcmp(cross(seg[i].S, seg[j].S))) {
        line[i].push_back(p.size());
        line[j].push_back(p.size());
        p.push_back(intersection(seg[i].F, seg[i].S, seg[j].F, seg[j].S));
      }
    }
  head.resize(p.size(), -1);
  for (int i = 0; i < n; ++i) {
    sort(line[i].begin(), line[i].end(),
         [&](int a, int b) { return p[a] < p[b]; });
    for (size_t j = 1; j < line[i].size(); ++j) {
      add(line[i][j], line[i][j - 1]);
    }
  }
  vector<double> as;
  mark.resize(e.size());
  const double pi = acos(-1);
  for (int i = 0; i < e.size(); ++i)
    if (!mark[i]) {
      int le = i ^ 1;
      int lp = e[i];
      int hd = e[le];
      mark[i] = true;
      double ans = cross(p[hd], p[lp]);
      while (lp != hd) {
        double best = 1e20;
        int cur = -1;
        double base = arg(p[e[le]] - p[lp]);
        for (int k = head[lp]; k != -1; k = nxt[k])
          if (k != le) {
            double tmp = arg(p[e[k]] - p[lp]) - base;
            while (tmp < 0) tmp += 2 * pi;
            while (tmp >= 2 * pi) tmp -= 2 * pi;
            if (tmp < best or cur == -1) {
              best = tmp;
              cur = k;
            }
          }
        if (best >= pi) break;
        ans += cross(p[lp], p[e[cur]]);
        lp = e[cur];
        le = cur ^ 1;
        mark[cur] = true;
      }
      if (lp == hd)
        as.push_back(fabs(ans) / 2);
    }
  sort(as.begin(), as.end());
  reverse(as.begin(), as.end());
  printf("%d %.12f %.12f\n", int(as.size()), as.front(), as.back());
  int m;
  scanf("%d", &m);
  for (int x; m--;) {
    scanf("%d", &x);
    if (x > (int)as.size())
      puts("Invalid question");
    else
      printf("%.12f\n", as[x - 1]);
  }
  return 0;
}
