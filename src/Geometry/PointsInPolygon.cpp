// Verify POJ 1265
#include <bits/stdc++.h>
using namespace std;
struct { int x, y; } a[1010]; int n;
double area() {};
int BorderIntPointNum() {
  int num = 0;
  a[n] = a[0];
  for (int i = 0; i < n; ++i)
    num += __gcd(abs(a[i + 1].x - a[i].x), abs(a[i + 1].y - a[i].y));
  return num;
}
int InsideIntPointNum() {
  return int(area()) + 1 - BorderIntPointNum() / 2;
}
int main() {
  return 0;
}

