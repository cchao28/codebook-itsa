#include <bits/stdc++.h>
using namespace std;
const int maxn = 12;
int fac[maxn];
void intToArray(int x, int a[maxn], int n) {
  int used[maxn] = {};
  for (int i = 1, j; i <= n; ++i) {
    int tmp = x / fac[n - i];
    for (j = 1; j <= n; ++j) if (!used[j]) {
      if (tmp == 0) break;
      --tmp;
    }
    a[i] = j;
    used[j] = true;
    x %= fac[n - i];
  }
}
int arrayToInt(int a[maxn], int n) {
  int ret = 0;
  for (int i = 1; i <= n; ++i) {
    int tmp = a[i] - 1;
    for (int j = 1; j <= i; ++j) if (a[j] < a[i]) --tmp;
    ret += fac[n - i] * tmp;
  }
  return ret;
}
int main() {
  for (int i = 0; i < maxn; ++i) fac[i] = i <= 1 ? 1 : i * fac[i - 1];
  int x, a[12], n = 10;
  while (scanf("%d", &x) == 1) {
    intToArray(x, a, n);
    for (int i = 1; i <= n; ++i) printf("%d ", a[i]); puts("");
    assert(arrayToInt(a, n) == x);
  }
  return 0;
}

