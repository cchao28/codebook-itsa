#include <cstdio>
#include <iostream>
using namespace std;
const int maxn = 100010;
int n, k, a[maxn], s[maxn] = {}; // a: original sequence(1 based), s: prefix sum
int q[maxn], front, rear;        // deque
#define qsz (rear - front)
inline bool useless(int a, int b, int c) {
  return 1LL * (s[c] - s[b]) * (b - a) <= 1LL * (s[b] - s[a]) * (c - b);
}
double solve() {
  double ans = 0;
  front = rear = 0;
  for (int i = k; i <= n; ++i) {
    while (qsz >= 2 && useless(q[rear - 2], q[rear - 1], i - k)) rear--;
    q[rear++] = i - k;
    while (qsz >= 2 && useless(q[front + 1], q[front], i)) front++;
    ans = max(ans, 1.0 * (s[i] - s[q[front]]) / (i - q[front]));
  }
  return ans;
}
int main() {
  while (scanf("%d%d", &n, &k) == 2) {
    for (int i = 1; i <= n; ++i) {
      scanf("%d", &a[i]);
      s[i] = a[i] + s[i - 1];
    }
    printf("%.2f\n", solve());
  }
  return 0;
}
