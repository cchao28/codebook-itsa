// hdu 1530
#include <cstdio>
void Clique(int n, int *u, int g[55][55], int sz, int &mx) {
  int vn, v[55];
  if (n && sz + n > mx) {
    for (int i = 0; i < n + sz - mx && i < n; ++i) {
      vn = 0;
      for (int j = i + 1; j < n; ++j)
        if (g[u[i]][u[j]])
          v[vn++] = u[j];
      Clique(vn, v, g, sz + 1, mx);
    }
  } else if (sz > mx)
    mx = sz;
}
int MaxClique(int n, int g[55][55]) {
  int ret = 0, vn, v[55];
  for (int i = n - 1; i >= 0; --i) {
    vn = 0;
    for (int j = i + 1; j < n; ++j)
      if (g[i][j])
        v[vn++] = j;
    Clique(vn, v, g, 1, ret);
  }
  return ret;
}
int main() {
  int n, g[55][55];
  while (scanf("%d", &n) == 1 && n) {
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
        scanf("%d", &g[i][j]);
    printf("%d\n", MaxClique(n, g));
  }
  return 0;
}
