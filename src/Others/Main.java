import java.io.*;
import java.util.*;
import java.math.*;
public class Main {
  void solve() {
  }
  Scanner in; PrintWriter out;
  void runFile(String task) {
    try {
      in = new Scanner(task + ".in");
      out = new PrintWriter(task + ".out");
    } catch(IOException e) {
      e.printStackTrace();
    }
    solve(); out.close();
  }
  void run() {
    in = new Scanner(System.in);
    out = new PrintWriter(System.out);
    solve();
    out.close();
  }
  public static void main(String[] args) { new Main().run(); }
}
