#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
struct Frac {
  ll u, l;
  Frac(ll u = 0, ll l = 1) : u(u), l(l) { maintain(); }
  void maintain() {
    ll g = __gcd(u, l);
    u /= g;
    l /= g;
    if (l < 0)
      l = -l, u = -u;
  }
  Frac operator+(const Frac &r) const {
    return Frac(u * r.l + l * r.u, l * r.l);
  }
  Frac operator-(const Frac &r) const {
    return Frac(u * r.l - l * r.u, l * r.l);
  }
  Frac operator*(const Frac &r) const { return Frac(u * r.u, l * r.l); }
  Frac operator/(const Frac &r) const { return Frac(u * r.l, l * r.u); }
  Frac operator-() const { return Frac(-u, l); }
};
ostream &operator<<(ostream &out, Frac f) {
  out << "(" << f.u << "/" << f.l << ")";
  return out;
}
typedef vector<Frac> Poly;
Poly operator+(const Poly &a, const Poly &b) {
  Poly ret(max(a.size(), b.size()));
  for (size_t i = 0; i < a.size(); ++i)
    ret[i] = ret[i] + a[i];
  for (size_t i = 0; i < b.size(); ++i)
    ret[i] = ret[i] + b[i];
  while (ret.size() > 1 and ret.back().u == 0)
    ret.pop_back();
  return ret;
}
Poly operator*(const Poly &a, const Poly &b) {
  Poly ret(a.size() + b.size());
  for (size_t i = 0; i < a.size(); ++i)
    for (size_t j = 0; j < b.size(); ++j)
      ret[i + j] = ret[i + j] + (a[i] * b[j]);
  while (ret.size() > 1 and ret.back().u == 0)
    ret.pop_back();
  return ret;
}
Poly operator*(const Poly &a, const Frac b) {
  Poly ret = a;
  for (size_t i = 0; i < ret.size(); ++i)
    ret[i] = ret[i] * b;
  while (ret.size() > 1 and ret.back().u == 0)
    ret.pop_back();
  return ret;
}
Frac b[30];
ll C[33][33];
int main() {
  for (int i = 0; i < 33; ++i)
    for (int j = 0; j <= i; ++j)
      C[i][j] = j ? C[i - 1][j - 1] + C[i - 1][j] : 1;
  b[0] = Frac(1);
  for (int m = 1; m < 30; ++m) {
    Frac sum(0);
    for (int j = 0; j < m; ++j) {
      sum = sum + Frac(C[m + 1][j]) * b[j];
    }
    b[m] = sum / Frac(-(m + 1));
  }
  for (int m; cin >> m;) {
    Poly res(m + 2);
    for (int i = 0; i <= m; ++i) {
      Poly tmp, r;
      tmp.push_back(Frac(1));
      tmp.push_back(Frac(1));
      r.push_back(Frac(1));
      for (int j = 0; j < m + 1 - i; ++j)
        r = r * tmp;
      res = res + (r * (b[i] * Frac(C[m + 1][i])));
    }
    res = res * Frac(1, m + 1);
    res.resize(m + 2);
    ll lcm = 1;
    for (int i = 0; i <= m + 1; ++i)
      lcm = lcm / __gcd(lcm, res[i].l) * res[i].l;
    cout << lcm;
    for (int i = m + 1; i >= 0; --i)
      cout << ' ' << res[i].u * (lcm / res[i].l);
    cout << endl;
  }
  return 0;
}
