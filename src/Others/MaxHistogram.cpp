/*
* input: array a with size n (indexed from 1)
* return: max retangle area of this histogram
*/
int MaxHistogram(int *a, int n) {
  stack<pair<int, int>> s;
  int ret = 0;
  for (int i = 1; i <= n; ++i) {
    int h = a[i], start = i;
    while (!s.empty() && h < s.top().second) {
      ret = max(ret, s.top().second * (i - s.top().first));
      start = s.top().first;
      s.pop();
    }
    if (s.empty() || h > s.top().second)
      s.push(make_pair(start, h));
  }
  for (; s.size(); s.pop())
    ret = max(ret, s.top().second * (n + 1 - s.top().first));
  return ret;
}
