#include <bits/extc++.h>
#include <bits/stdc++.h>
// #include <ext/pb_ds/assoc_container.hpp>
// #include <ext/pb_ds/tree_policy.hpp>
using namespace std;
using namespace __gnu_cxx;
using namespace __gnu_pbds;
template <typename T>
using pbds_set = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
using heap = __gnu_pbds::priority_queue<int>;
template <typename T>
using hyperset = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <typename T, typename U>
using hypermap = tree<T, U, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
void test_rb_tree() {
  pbds_set<int> s;
  s.insert(0);
  s.insert(123);
  s.insert(12);
  s.insert(12345);
  assert(*s.find_by_order(1) == 12);
  s.erase(0);
  assert(s.order_of_key(12) == 0);
}
int main() {
  test_rb_tree();
  return 0;
}
