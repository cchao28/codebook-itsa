// HDU 2191
typedef pair<int, int> pii;
void add(int f[], int V, int v, int n, int w) {
  n = min(n, V / v);
  deque<pii> q;
  for (int b = 0; b < v; ++b) {
    q.clear();
    for (int a = 0, j = b; j <= V; ++a, j += v) {
      int t = f[j] - a * w;
      while (q.size() && q.back().first < t)
        q.pop_back();
      q.push_back(pii(t, a));
      while (q.size() && q.front().second < a - n)
        q.pop_front(); // a - F + 1 > n + 1
      f[j] = q.front().first + a * w;
    }
  }
}
