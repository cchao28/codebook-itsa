const int maxn = 100000 + 10;
#define rep(i, n) for (int i = 0; i < (int)(n); ++i)
char s[maxn];
int n, pos[maxn], sa[maxn], rk[maxn], height[maxn], gap, tmp[maxn];
bool cmp(int i, int j) {
  if (pos[i] != pos[j])
    return pos[i] < pos[j];
  i += gap;
  j += gap;
  return (i < n && j < n) ? pos[i] < pos[j] : i > j;
}
void build_sa() {
  rep(i, n) sa[i] = i, pos[i] = s[i];
  for (gap = 1;; gap += gap) {
    sort(sa, sa + n, cmp);
    tmp[0] = 0;
    rep(i, n - 1) tmp[i + 1] = tmp[i] + cmp(sa[i], sa[i + 1]);
    rep(i, n) pos[sa[i]] = tmp[i];
    if (tmp[n - 1] == n - 1)
      break;
  }
}
