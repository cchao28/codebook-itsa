// 最小循環字串
unsigned minCircular(string s) {
  unsigned i, j, k, l, N = s.size();
  s += s;
  for (i = 0, j = 1; j < N;) {
    for (k = 0; k < N && s[i + k] == s[j + k]; ++k)
      ;
    if (k >= N)
      break;
    if (s[i + k] < s[j + k])
      j += k + 1;
    else {
      l = i + k;
      i = j;
      j = max(l, j) + 1;
    }
  }
  return i;
}
