/**
* input: s l
* result: f: lcp of every suffix(except the origin string)
*/
void exkmp(char *s, int *f, int l) {
  f[0] = 0;
  for (int i = 1, j = 0; i < l; ++i) {
    f[i] = j + f[j] > i ? min(f[i - j], j + f[j] - i) : 0;
    while (s[f[i]] == s[i + f[i]])
      ++f[i];
    if (f[i] + i > f[j] + j)
      j = i;
  }
}
