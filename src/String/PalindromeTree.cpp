const int maxn = 200010;
struct Node {
  Node *ch[26], *f;
  int len, cnt;
  Node(int len = 0) {
    memset(ch, 0, sizeof ch);
    f = NULL;
    cnt = 0;
    this->len = len;
  }
};
struct PT {
  Node pool[maxn];
  int sz;
  Node *newnode(int len = 0) {
    pool[sz] = Node(len);
    return &pool[sz++];
  }
  Node *last, *m1;
  int s[maxn];
  int n;
  void init() {
    sz = 0;
    last = newnode(0);
    last->f = newnode(-1);
    pool[1].f = last->f;
    s[n = 0] = -1;
  }
  Node *get_fail(Node *x) {
    while (s[n - x->len - 1] != s[n])
      x = x->f;
    return x;
  }
  void add(int c) {
    s[++n] = c;
    Node *u = get_fail(last);
    if (!u->ch[c]) {
      Node *p = newnode(u->len + 2);
      p->f = get_fail(u->f)->ch[c];
      if (!p->f)
        p->f = &pool[0];
      u->ch[c] = p;
    }
    last = u->ch[c];
    last->cnt++;
  }
  void count() {
    for (int i = sz - 1; i > 1; --i)
      pool[i].f->cnt += pool[i].cnt;
    pool[0].cnt = pool[1].cnt = 0;
  }
} pt;
