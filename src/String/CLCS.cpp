// 100203B - Bracelets
#include <bits/stdc++.h>
using namespace std;
const int LEFT = 0, DIAG = 1, UP = 2;
int d[4000][4000], p[4000][4000] = {{0}};
void LCS(char *A, char *B, int m, int n) {
  for (int i = 1; i <= m; ++i) {
    for (int j = 1; j <= n; ++j) {
      d[i][j] = 0, p[i][j] = 0;
      int a[] = {d[i][j - 1], A[i] == B[j] ? d[i - 1][j - 1] + 1 : 0,
                 d[i - 1][j]};
      for (int k = 0; k < 3; ++k)
        if (a[k] > d[i][j]) {
          d[i][j] = a[k];
          p[i][j] = k;
        }
    }
  }
}
int Trace(int m, int n) {
  int ret = 0;
  while (m > 0 && n > 0) {
    if (p[m][n] == LEFT) n--;
    else if (p[m][n] == UP) m--;
    else { n--; m--; ret++; }
  }
  return ret;
}
void Reroot(int root, int m, int n) {
  int i = root, j = 1;
  while (j <= n && p[i][j] != DIAG)
    j++;
  if (j > n)
    return;
  p[i][j] = LEFT;
  while (i < 2 * m && j < n) {
    if (p[i + 1][j] == UP) {
      i++;
      p[i][j] = LEFT;
    } else if (p[i + 1][j + 1] == DIAG) {
      i++;
      j++;
      p[i][j] = LEFT;
    } else
      j++;
  }
  while (i < 2 * m && p[i + 1][j] == UP) {
    i++;
    p[i][j] = LEFT;
  }
}
int CLCS(char *A, char *B) {
  int m = strlen(A + 1), n = strlen(B + 1);
  memset(d, 0, sizeof d);
  char AA[4000];
  strcpy(AA + 1, A + 1);
  strcpy(AA + 1 + m, A + 1);
  LCS(AA, B, m * 2, n);
  int S = Trace(m, n);
  for (int i = 1; i < m; ++i) {
    Reroot(i, m, n);
    S = max(S, Trace(m + i, n));
  }
  return S;
}
char A[2000], B[2000];
int main() {
  scanf("%s%s", A + 1, B + 1);
  int ans = CLCS(A, B);
  reverse(B + 1, B + 1 + strlen(B + 1));
  ans = max(ans, CLCS(A, B));
  printf("%d\n", ans * 2);
  return 0;
}
