struct Node {
  int key;
  Node *f, *last, *ch[26];
  Node() : key(0), f(NULL), last(NULL) { memset(ch, 0, sizeof ch); }
};
struct AC {
  Node *root;
  AC() { root = NULL; }
  void init() { root = new Node(); }
  void insert(char *s, int key) {
    Node *cur = root;
    for (int i = 0; s[i]; ++i) {
      int idx = s[i] - 'a';
      if (!cur->ch[idx])
        cur->ch[idx] = new Node();
      cur = cur->ch[idx];
    }
    cur->key = key;
  }
  void build() {
    queue<Node *> q;
    root->f = root->last = root;
    q.push(root);
    while (q.size()) {
      Node *x = q.front();
      q.pop();
      for (int i = 0; i < 26; ++i)
        if (x->ch[i]) {
          Node *y = x->ch[i];
          q.push(y);
          y->f = y->last = root;
          Node *t = x->f;
          while (t != root && !t->ch[i])
            t = t->f;
          if (x != root) {
            y->f = t->ch[i] ? t->ch[i] : root;
            y->last = y->f->key ? y->f : y->f->last;
          }
        }
    }
  }
  int cnt[220]; // number of string
  void print(Node *x) {
    if (x->key) {
      cnt[x->key]++;
      print(x->last);
    }
  }
  void query(char *s) {
    Node *cur = root;
    for (int i = 0; s[i]; ++i) {
      int idx = s[i] - 'a';
      while (cur != root && !cur->ch[idx])
        cur = cur->f;
      if (cur->ch[idx]) {
        cur = cur->ch[idx];
        if (cur->key)
          print(cur);
        else if (cur->last != root)
          print(cur->last);
      }
    }
  }
} solver;
