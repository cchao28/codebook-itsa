struct Node {
  int len;
  Node *ch[26], *f;
  Node() : len(0), f(NULL) { memset(ch, 0, sizeof ch); }
};
struct SAM {
  Node *last, *root;
  SAM() { root = last = new Node(); };
  void add(int c) {
    Node *e = new Node(), *tmp = last;
    e->len = last->len + 1;
    for (; tmp && !tmp->ch[c]; tmp = tmp->f)
      tmp->ch[c] = e;
    if (!tmp) {
      e->f = root;
    } else {
      Node *nxt = tmp->ch[c];
      if (tmp->len + 1 == nxt->len)
        e->f = nxt;
      else {
        Node *np = new Node();
        *np = *nxt;
        np->len = tmp->len + 1;
        nxt->f = e->f = np;
        for (; tmp && tmp->ch[c] == nxt; tmp = tmp->f)
          tmp->ch[c] = np;
      }
    }
    last = e;
  }
};
