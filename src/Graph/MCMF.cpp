#include <bits/stdc++.h>
using namespace std;
const int maxn = 110, inf = INT_MAX;
#define EB emplace_back
typedef pair<int, int> pii;
struct MCMF {
  struct Edge {
    int from, to, cap, cost, flow;
    Edge(int from, int to, int cap, int cost, int flow = 0)
        : from(from), to(to), cap(cap), cost(cost), flow(flow){};
  };
  int n;
  vector<int> g[maxn];
  vector<Edge> edges;
  void init(int n) {
    this->n = n;
    for (int i = 0; i < n; ++i)
      g[i].clear();
    edges.clear();
  }
  void add(int from, int to, int cap, int cost) {
    int m = edges.size();
    g[from].EB(m);
    g[to].EB(m + 1);
    edges.EB(from, to, cap, cost);
    edges.EB(to, from, 0, -cost);
  }
  bool inq[maxn];
  int dis[maxn], prev[maxn], aug[maxn];
  queue<int> q;
  bool spfa(int s, int t, int &cost, int &flow) {
    for (int i = 0; i < n; ++i)
      dis[i] = inf;
    dis[s] = 0;
    aug[s] = inf;
    q.push(s);
    while (q.size()) {
      int x = q.front();
      q.pop();
      inq[x] = false;
      for (int i : g[x]) {
        Edge &e = edges[i];
        if (e.cap > e.flow && dis[e.from] + e.cost < dis[e.to]) {
          dis[e.to] = dis[e.from] + e.cost;
          prev[e.to] = i;
          aug[e.to] = min(aug[x], e.cap - e.flow);
          if (!inq[e.to]) {
            q.push(e.to);
            inq[e.to] = true;
          }
        }
      }
    }
    if (dis[t] == inf)
      return false;
    cost += aug[t] * dis[t];
    flow += aug[t];
    for (int x = t; x != s; x = edges[prev[x]].from) {
      int i = prev[x];
      edges[i].flow += aug[t];
      edges[i ^ 1].flow -= aug[t];
    }
    return true;
  }
  pii minCost(int s, int t) {
    int cost = 0, flow = 0;
    while (spfa(s, t, cost, flow))
      ;
    return {cost, flow};
  }
} sol;
int main() {}
