// 最大密度子圖
Dinic sol;
int n, m, a[maxn*maxn], b[maxn*maxn], d[maxn] = {}, s, t;
void build(double g) {
	sol.clearAll(n + 2);
	for(int i = 1; i <= n; ++i) {
		sol.addEdge(s, i, m);
		sol.addEdge(i, t, m + 2 * g - d[i]);
	}
	for(int i = 0; i < m; ++i) {
		sol.addEdge(a[i], b[i], 1);
		sol.addEdge(b[i], a[i], 1);
	}
}
long long arr[maxn];
bool adj[maxn][maxn];
int main() {
	int T; scanf("%d", &T); while(T--) {
		scanf("%d", &n);
		s = 0, t = n + 1;
		for(int i = 1; i <= n; ++i) {
			scanf("%lld", &arr[i]);
			d[i] = 0;
		}
		memset(adj, 0, sizeof adj);
		m = 0;
		for(int i = 1; i <= n; ++i)
			for(int j = 1; j < i; ++j)
				if(__gcd(arr[i], arr[j]) > 1) {
					a[m] = i; b[m] = j;
					d[i]++, d[j]++;
					adj[i][j] = adj[j][i] = true;
					m++;
				}
		double l = 0, r = m, stop = 1.0 / n / n;
		while(r - l >= stop) {
			double mid = (l + r) / 2;
			build(mid);
			if((n * m - sol.maxFlow(s, t)) / 2 > eps) l = mid;
			else r = mid;
		}
		build(l);
		sol.maxFlow(s, t);
		vector<int> ans;
		for(int i = 1; i <= n; ++i) if(sol.vis[i]) ans.push_back(i);
		int nn = ans.size(), mm = 0;
		if(nn) {
			for(unsigned i = 0; i < ans.size(); ++i)
				for(unsigned j = 0; j < i; ++j)
					if(adj[ans[i]][ans[j]])
						mm++;
			int gg = __gcd(nn, mm);
			nn /= gg, mm /= gg;
		}
		else {nn = 1, mm = 0; }
		printf("%d/%d\n", mm, nn);
	}
	return 0;
}