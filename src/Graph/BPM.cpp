const int maxn = 110;
vector<int> g[maxn];
int n, m, l[maxn];
bool vis[maxn];
bool match(int x) {
  for (int y : g[x])
    if (!vis[y]) {
      vis[y] = true;
      if (l[y] == -1 || match(l[y])) {
        l[y] = x;
        return true;
      }
    }
  return false;
}
int solve() {
  memset(l, -1, sizeof l);
  int ans = 0;
  for (int i = 0; i < n; ++i) {
    memset(vis, 0, sizeof vis);
    if (match(i))
      ans++;
  }
  return ans;
}
