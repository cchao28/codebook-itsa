// 度序列判斷
// @param n: number of vertices.
// @param a: (degree, no of vertice in [0, n - 1])
typedef pair<int, int> PII;
#define F first
#define S second
int n;
PII a[20];
bool g[20][20];
bool build() {
  memset(g, 0, sizeof g);
  for (;;) {
    sort(a, a + n, greater<PII>());
    if (a[0].F == 0)
      return true;
    int t = a[0].F;
    if (t >= n)
      return false;
    for (int i = 1; i <= t; ++i) {
      int x = a[0].S;
      int y = a[i].S;
      g[x][y] = g[y][x] = true;
      if (--a[i].F < 0)
        return false;
    }
    a[0].F = 0;
  }
}
