#include <bits/stdc++.h>
using namespace std;
const int maxn = 1010;
vector<int> construct(int n, vector<int> adj[maxn]) {
  static int rank[maxn], label[maxn];
  memset(rank, -1, sizeof rank);
  memset(label, 0, sizeof label);
  priority_queue<pair<int, int>> heap;
  for (int i = 0; i < n; ++i) heap.push({0, i});
  for (int i = n - 1; i >= 0; --i) for (;;) {
    int u = heap.top().second; heap.pop();
    if (rank[u] == -1) {
      rank[u] = i;
      for (auto x : adj[u]) if (rank[x] == -1) {
        label[x]++;
        heap.push({label[x], x});
      }
      break;
    }
  }
  vector<int> result(n);
  for (int i = 0; i < n; ++i)
    result[rank[i]] = i;
  return result;
}
bool check(int n, vector<int> adj[maxn], vector<int> ord) {
  static bool mark[maxn];
  static int rank[maxn];
  for (int i = 0; i < n; ++i) rank[ord[i]] = i;
  memset(mark, 0, sizeof mark);
  for (int i = 0; i < n; ++i) {
    vector<pair<int, int>> tmp;
    for (auto x : adj[ord[i]]) if (!mark[x])
      tmp.push_back({rank[x], x});
    sort(tmp.begin(), tmp.end());
    if (tmp.size()) {
      int u = tmp[0].second;
      set<int> tmpAdj;
      for (auto x : adj[u])
        tmpAdj.insert(x);
      for (int i = 1; i < tmp.size(); ++i)
        if (!tmpAdj.count(tmp[i].second))
          return false;
    }
    mark[ord[i]] = true;
  }
  return true;
}
bool is_chordal(int n, vector<pair<int, int>> edges) {
  vector<int> adj[maxn];
  for (auto it : edges) {
    adj[it.first].push_back(it.second);
    adj[it.second].push_back(it.first);
  }
  return check(n, adj, construct(n, adj));
}
int main() {
  // ZOJ 1015
  int n, m;
  while (scanf("%d%d", &n, &m) == 2) {
    if (n == 0 && m == 0) break;
    vector<pair<int, int>> edges;
    for (int i = 0, u, v; i < m; ++i) {
      scanf("%d%d", &u, &v);
      edges.emplace_back(u - 1, v - 1);
    }
    puts(is_chordal(n, edges) ? "Perfect\n" : "Imperfect\n");
  }
  return 0;
}
