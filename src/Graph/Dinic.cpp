#include <bits/stdc++.h>
using namespace std;
const int maxn = 100 + 10;
const int inf = 1000000000;
struct Dinic {
  struct Edge {
    int from, to, cap, flow;
    Edge(int from, int to, int cap, int flow = 0)
        : from(from), to(to), cap(cap), flow(flow) {}
  };
  int n, m, s, t;
  vector<Edge> edges;
  vector<int> G[maxn];
  bool vis[maxn];
  int d[maxn];
  unsigned cur[maxn];
  void clearAll(int n) {
    for (int i = 0; i < n; ++i)
      G[i].clear();
    edges.clear();
  }
  void addEdge(int from, int to, int cap) {
    edges.emplace_back(from, to, cap);
    edges.emplace_back(to, from, 0);
    m = edges.size();
    G[from].push_back(m - 2);
    G[to].push_back(m - 1);
  }
  bool bfs() {
    memset(vis, 0, sizeof vis);
    queue<int> q;
    q.push(s);
    vis[s] = true;
    d[s] = 0;
    while (q.size()) {
      int x = q.front();
      q.pop();
      for (unsigned i = 0; i < G[x].size(); ++i) {
        Edge &e = edges[G[x][i]];
        if (!vis[e.to] && e.cap > e.flow) {
          vis[e.to] = true;
          d[e.to] = d[x] + 1;
          q.push(e.to);
        }
      }
    }
    return vis[t];
  }
  int dfs(int x, int a) {
    if (x == t || a == 0)
      return a;
    int flow = 0, f;
    for (unsigned &i = cur[x]; i < G[x].size(); ++i) {
      Edge &e = edges[G[x][i]];
      if (d[e.to] == d[x] + 1 && (f = dfs(e.to, min(a, e.cap - e.flow))) > 0) {
        e.flow += f;
        edges[G[x][i] ^ 1].flow -= f;
        flow += f;
        a -= f;
        if (a == 0)
          break;
      }
    }
    return flow;
  }
  int maxFlow(int s, int t) {
    this->s = s;
    this->t = t;
    int flow = 0;
    while (bfs()) {
      memset(cur, 0, sizeof cur);
      flow += dfs(s, inf);
    }
    return flow;
  }
};
int main() {}
