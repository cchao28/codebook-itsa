//朱劉算法 最小樹形圖
const int maxm = 40010;
struct Edge {
  int from, to, w;
} e[maxm];
int sz = 0;
void add(int a, int b, int c) { e[sz++] = (Edge){a, b, c}; }
const int maxn = 1010;
int V, in[maxn], fa[maxn], no[maxn], v[maxn];
bool removed[maxn];
int mst(int r) {
  int w1 = 0, w2 = 0;
  memset(removed, 0, sizeof removed);
  for (;;) {
    w1 = 0;
    memset(in, 0x7f, sizeof in);
    memset(fa, -1, sizeof fa);
    memset(no, -1, sizeof no);
    memset(v, -1, sizeof v);
    for (int i = 0; i < sz; ++i) {
      if (e[i].from != e[i].to && e[i].w < in[e[i].to]) {
        fa[e[i].to] = e[i].from;
        in[e[i].to] = e[i].w;
      }
    }
    bool loop = false;
    for (int i = 0; i < V; ++i) {
      if (removed[i] || i == r)
        continue;
      if (fa[i] == -1)
        return -1;
      else
        w1 += in[i];
      int s = i;
      while (s != r && v[s] == -1) {
        v[s] = i;
        s = fa[s];
      }
      if (s != r && v[s] == i) {
        loop = true;
        for (int j = s;;) {
          no[j] = s;
          removed[j] = true;
          w2 += in[j];
          j = fa[j];
          if (j == s)
            break;
        }
        removed[s] = false;
      }
    }
    if (!loop)
      break;
    for (int i = 0; i < sz; ++i) {
      Edge &x = e[i];
      if (no[x.to] >= 0)
        x.w -= in[x.to];
      if (no[x.to] >= 0)
        x.to = no[x.to];
      if (no[x.from] >= 0)
        x.from = no[x.from];
      if (x.from == x.to)
        e[i--] = e[--sz];
    }
  }
  return w1 + w2;
}
