/* Dominator Tree for Codechef GRAPHCNT
 * Given a directed graph in vector<int> g[], and n.
 * Find the dominator tree.
 * Example usage: dfs(1); DominatorTree();
 * Reference: http://codeforces.com/blog/entry/22811
 */
const size_t maxn = 100010;
vector<int> g[maxn], gr[maxn];
int n, m;
int dfn[maxn], rev[maxn], timer = 0;
int pa[maxn], label[maxn], sdom[maxn], dom[maxn], dsu[maxn];
void dfs(int x) {
  dfn[x] = ++timer; rev[timer] = x;
  label[timer] = sdom[timer] = dsu[timer] = timer;
  for (int y : g[x]) {
    if (!dfn[y]) {
      dfs(y);
      pa[dfn[y]] = dfn[x];
    }
    gr[dfn[y]].push_back(dfn[x]);
  }
}
int Find(int u, int x = 0) {
  if (u == dsu[u]) return x ? -1 : u;
  int v = Find(dsu[u], x + 1);
  if (v < 0) return u;
  if (sdom[label[dsu[u]]] < sdom[label[u]])
    label[u] = label[dsu[u]];
  dsu[u] = v;
  return x ? v : label[u];
}
void Union(int u, int v) {
  dsu[v] = u;
}
vector<int> bucket[maxn], tree[maxn];
void DominatorTree() {
  for (int i = n; i >= 1; --i) {
    for (int y : gr[i]) sdom[i] = min(sdom[i], sdom[Find(y)]);
    if (i > 1) bucket[sdom[i]].push_back(i);
    for (int y : bucket[i]) {
      int v = Find(y);
      dom[y] = sdom[y] == sdom[v] ? sdom[y] : v;
    }
    if (i > 1) Union(pa[i], i);
  }
  for (int i = 2; i <= n; ++i) {
    if (dom[i] != sdom[i]) dom[i] = dom[dom[i]];
    tree[rev[dom[i]]].push_back(rev[i]);
  }
}
