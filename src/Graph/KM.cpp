#include <bits/stdc++.h>
using namespace std;
const int maxn = 510, inf = INT_MAX;
struct KM {
  int n, w[maxn][maxn];
  int lx[maxn], ly[maxn], match[maxn], slack[maxn];
  bool s[maxn], t[maxn];
  bool hungary(int x) {
    s[x] = true;
    for (int y = 0; y < n; ++y)
      if (!t[y] && lx[x] + ly[y] == w[x][y]) {
        t[y] = true;
        if (match[y] == -1 || hungary(match[y])) {
          match[y] = x;
          return true;
        }
      } else
        slack[y] = min(slack[y], lx[x] + ly[y] - w[x][y]);
    return false;
  }
  int solve() {
    int ret = 0;
    fill(match, match + n, -1);
    fill(ly, ly + n, 0);
    for (int i = 0; i < n; ++i)
      lx[i] = *max_element(w[i], w[i] + n);
    for (int i = 0; i < n; ++i) {
      for (;;) {
        fill(s, s + n, false);
        fill(t, t + n, false);
        fill(slack, slack + n, inf);
        if (hungary(i))
          break;
        else {
          int a = inf;
          for (int j = 0; j < n; ++j)
            if (!t[j] && slack[j] < a)
              a = slack[j];
          for (int j = 0; j < n; ++j) {
            if (s[j])
              lx[j] -= a;
            if (t[j])
              ly[j] += a;
          }
        }
      }
    }
    for (int i = 0; i < n; ++i)
      ret += w[match[i]][i];
    return ret;
  }
} sol;
