#include <bits/stdc++.h>
using namespace std;
vector<int> g[1010];
string dfs(int x, int p) {
  vector<string> child;
  for (int y : g[x])
    if (y != x)
      child.push_back(dfs(y, x));
  sort(child.begin(), child.end());
  string ret = "(";
  for (string &s : child)
    ret += s;
  ret += ")";
  return ret;
}
int main() {}
