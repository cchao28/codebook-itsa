const int maxn = 111111;
const int inf = 0x3fffffff;
inline int lowbit(const int &x) { return x & -x; }
struct Edge {
  int u, v, c;
  Edge(int _u = 0, int _v = 0, int _c = 0) : u(_u), v(_v), c(_c) {}
} edge[maxn * 4];
inline bool operator<(const Edge &a, const Edge &b) { return a.c < b.c; }
struct Node {
  int key, id;
  Node(int k = 0, int i = 0) : key(k), id(i) {}
} Tree[maxn];
inline bool operator<(const Node &a, const Node &b) { return a.key < b.key; }
int IDx[maxn], IDy[maxn], bak[maxn];
int x[maxn], y[maxn], id[maxn], father[maxn], n;
int fnd(const int &x) {
  return father[x] == x ? x : father[x] = fnd(father[x]);
}
inline bool cmp1(const int &i, const int &j) {
  return x[i] - y[i] > x[j] - y[j] || x[i] - y[i] == x[j] - y[j] && y[i] > y[j];
}
inline bool cmp2(const int &i, const int &j) {
  return x[i] - y[i] < x[j] - y[j] || x[i] - y[i] == x[j] - y[j] && y[i] > y[j];
}
inline bool cmp3(const int &i, const int &j) {
  return x[i] + y[i] < x[j] + y[j] || x[i] + y[i] == x[j] + y[j] && y[i] > y[j];
}
inline bool cmp4(const int &i, const int &j) {
  return x[i] + y[i] < x[j] + y[j] || x[i] + y[i] == x[j] + y[j] && y[i] < y[j];
}
inline void Process(int x[], int idx[], int n) {
  for (int i = 0; i < n; ++i)
    bak[i] = x[i];
  sort(bak, bak + n, greater<int>());
  int p = unique(bak, bak + n) - bak;
  for (int i = 0; i < n; ++i)
    idx[i] = lower_bound(bak, bak + p, x[i], greater<int>()) - bak + 1;
}
inline void add_edge(int &N, const int &u, const int &v) {
  edge[N++] = Edge(u, v, abs(x[u] - x[v]) + abs(y[u] - y[v]));
}
inline int get_min(const int &p) {
  Node tmp(inf);
  for (int i = p; i; i ^= lowbit(i))
    if (Tree[i].id != -1)
      tmp = min(tmp, Tree[i]);
  return tmp.key == inf ? -1 : tmp.id;
}
inline void insert(const int &n, const int &p, const Node &it) {
  for (int i = p; i <= n; i += lowbit(i))
    if (Tree[i].id == -1 || it < Tree[i])
      Tree[i] = it;
}
void Build(int x[], int y[], int ids[], int n, int wx, int wy, int &N,
           bool (*cmp)(const int &i, const int &j)) {
  for (int i = 0; i < n; ++i)
    id[i] = i;
  sort(id, id + n, cmp);
  for (int i = 1; i <= n; ++i)
    Tree[i].id = -1;
  for (int i = 0; i < n; ++i) {
    int u = id[i], v = get_min(ids[u]);
    if (v != -1)
      add_edge(N, u, v);
    insert(n, ids[u], Node(x[u] * wx + y[u] * wy, u));
  }
}
inline long long solve(int x[], int y[], int n) {
  Process(x, IDx, n);
  Process(y, IDy, n);
  int N = 0;
  Build(x, y, IDy, n, 1, 1, N, cmp1);
  Build(x, y, IDx, n, 1, 1, N, cmp2);
  Build(x, y, IDy, n, -1, 1, N, cmp3);
  Build(x, y, IDx, n, 1, -1, N, cmp4);
  sort(edge, edge + N);
  for (int i = 0; i < n; ++i)
    father[i] = i;
  long long res = 0;
  for (int i = 0; i < N; ++i) {
    int u = fnd(edge[i].u), v = fnd(edge[i].v);
    if (u != v) {
      father[u] = v;
      res += edge[i].c;
    }
  }
  return res;
}
