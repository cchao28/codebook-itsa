#include <bits/stdc++.h>
using namespace std;
const int maxn = 100010;
int n, dep[maxn], p[maxn], heavy[maxn], size[maxn];
vector<int> g[maxn];
int dfs(int x, int fa, int depth) {
  dep[x] = depth;
  p[x] = fa;
  size[x] = 1;
  heavy[x] = -1;
  for (int y : g[x])
    if (y != fa) {
      size[x] += dfs(y, x, depth + 1);
      if (heavy[x] == -1 || size[y] > size[heavy[x]])
        heavy[x] = y;
    }
  return size[x];
}
int root[maxn], cno[maxn], cnt, idx, no[maxn];
void hld() {
  cnt = 0;
  idx = 0;
  for (int i = 0; i < n; ++i)
    if (p[i] == -1 || heavy[p[i]] != i) {
      for (int j = i; j != -1; j = heavy[j]) {
        no[j] = idx++;
        cno[j] = cnt;
        root[j] = i;
      }
      cnt++;
    }
}
int lca(int a, int b) {
  while (cno[a] != cno[b])
    if (dep[root[a]] > dep[root[b]])
      a = p[root[a]];
    else
      b = p[root[b]];
  return dep[a] < dep[b] ? a : b;
}
int main() {}
