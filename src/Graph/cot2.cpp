#include <bits/stdc++.h>
using namespace std;
int n, m, kind;
vector<int> g[40010];
struct Q { int F, S, id; };
vector<Q> q;
int size;
int tot, st[40010], top, timer, dep[40010], in[40010], p[40010];
int a[40010], b[40010];
int dfs(int x) {
  int ret = 0;
  st[++top] = x, in[x] = ++timer;
  for (int y : g[x]) if (y != p[x]) {
    p[y] = x;
    dep[y] = dep[x] + 1;
    ret += dfs(y);
    if (ret >= size) {
      ++tot, ret = 0;
      while (st[top] != x) {
        b[st[top--]] = tot;
      }
    }
  }
  return ret + 1;
}
bool inpath[40010];
int cross, cnt[40010], ans;
void inv(int x) {
  if (inpath[x]) {
    inpath[x] = false;
    ans -= (--cnt[a[x]]) == 0;
  } else {
    inpath[x] = true;
    ans += (cnt[a[x]]++) == 0;
  }
}
void mv_up(int &x) {
  if (!cross) {
    if (inpath[x] && !inpath[p[x]]) cross = x;
    else if (!inpath[x] && inpath[p[x]]) cross = p[x];
  }
  inv(x), x = p[x];
}
void mv(int x, int y) {
  if (x == y) return ;
  cross = 0;
  if (inpath[y]) cross = y;
  while (dep[x] > dep[y]) mv_up(x);
  while (dep[x] < dep[y]) mv_up(y);
  while (x != y) mv_up(x), mv_up(y);
  inv(x), inv(cross);
}
int main() {
  scanf("%d%d", &n, &m);
  size = n / sqrt(m);
  map<int, int> f;
  for (int i = 1, x; i <= n; ++i) {
    scanf("%d", &x);
    if (!f.count(x)) f[x] = kind++;
    a[i] = f[x];
  }
  for (int i = 0, u, v; i < n - 1; ++i) {
    scanf("%d%d", &u, &v);
    g[u].push_back(v);
    g[v].push_back(u);
  }
  q.resize(m);
  for (int i = 0; i < m; ++i) {
    scanf("%d%d", &q[i].F, &q[i].S);
    q[i].id = i;
  }
  dfs(1);
  while (top) b[st[top--]] = tot;
  for (auto &x : q) {
    if (b[x.F] > b[x.S]) swap(x.F, x.S);
  }
  sort(q.begin(), q.end(), [&](Q q1, Q q2) {
    return tie(b[q1.F], in[q1.S]) < tie(b[q2.F], in[q2.S]);
  });
  int u = 1, v = 1;
  ans = 1; inpath[1] = true; cnt[a[1]] = 1;
  static int res[100010];
  for (auto &x : q) {
    mv(u, x.F); u = x.F;
    mv(v, x.S); v = x.S;
    res[x.id] = ans;
  }
  for (int i = 0; i < m; ++i) printf("%d\n", res[i]);
  return 0;
}
