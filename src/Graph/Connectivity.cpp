#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> pii;
const int maxn = 1010;
vector<int> g[maxn];
int pre[maxn], low[maxn], timer;

//無向圖割點: 存在一個後代不能連回祖先, 根只有一個小孩不能算
bool isap[maxn];
//無向圖橋: 後代不能連回自己
vector<pii> bridges;
//無向圖BCC
stack<pii> s;
vector<int> bcc[maxn];
int bccno[maxn], bcc_cnt;
int dfs(int x, int fa) {
  pre[x] = low[x] = ++timer;
  isap[x] = false;
  int child = 0;
  for (int y : g[x]) {
    if (!pre[y]) {
      s.push(pii(x, y));
      child++;
      low[x] = min(low[x], dfs(y, x));
      if (low[y] >= pre[x]) {
        isap[x] = true;
        bcc_cnt++;
        bcc[bcc_cnt].clear();
        for (;;) {
          pii e = s.top();
          s.pop();
          if (bccno[e.first] != bcc_cnt) {
            bcc[bcc_cnt].push_back(e.first);
            bccno[e.first] = bcc_cnt;
          }
          if (bccno[e.second] != bcc_cnt) {
            bcc[bcc_cnt].push_back(e.second);
            bccno[e.second] = bcc_cnt;
          }
          if (e.first == x && e.second == y)
            break;
        }
      }
      if (low[y] > pre[x])
        bridges.push_back(pii(x, y));
    } else if (pre[y] < pre[x] && y != fa) {
      s.push(pii(x, y));
      low[x] = min(low[x], pre[y]);
    }
  }
  if (fa == -1 && child == 1)
    isap[x] = 0;
  return low[x];
}
void undirected(int n) {
  memset(pre, 0, sizeof pre);
  memset(isap, 0, sizeof isap);
  memset(bccno, 0, sizeof bccno);
  timer = bcc_cnt = 0;
  for (int i = 0; i < n; ++i)
    if (!pre[i])
      dfs(i, -1);
}
//有向圖SCC
int sccno[maxn], scc_cnt;
stack<int> S;
int dfs(int x) {
  pre[x] = low[x] = ++timer;
  S.push(x);
  for (int y : g[x]) {
    if (!pre[y])
      low[x] = min(low[x], dfs(y));
    else if (!sccno[y])
      low[x] = min(low[x], pre[y]);
  }
  if (low[x] == pre[x]) {
    scc_cnt++;
    for (;;) {
      int t = S.top();
      S.pop();
      sccno[t] = scc_cnt;
      if (t == x)
        break;
    }
  }
  return low[x];
}
void directed(int n) {
  memset(pre, 0, sizeof pre);
  memset(sccno, 0, sizeof sccno);
  timer = scc_cnt = 0;
  for (int i = 0; i < n; ++i)
    if (!pre[i])
      dfs(i);
}

int main() {}
