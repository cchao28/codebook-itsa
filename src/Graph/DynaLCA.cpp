// SPOJ DYNALCA
#include <bits/stdc++.h>
using namespace std;
const int maxn = 100000 + 10;
int c[2][maxn], p[maxn], *l = c[0], *r = c[1];
inline int sgn(int x) { return l[p[x]] == x ? 0 : r[p[x]] == x ? 1 : -1; }
inline void setc(int x, int d, int y) { p[c[d][x] = y] = x; }
inline void rot(int x, int d) {
  int y = p[x], z = p[y];
  if (~sgn(y))
    setc(z, sgn(y), x);
  else
    p[x] = z;
  setc(y, d, c[!d][x]), setc(x, !d, y);
}
inline int splay(int x) {
  int a, b, y;
  while (~(a = sgn(x))) {
    if (~(b = sgn(y = p[x])))
      rot(a ^ b ? x : y, a), rot(x, b);
    else
      rot(x, a);
  }
  return x;
}
int acs(int _x) {
  int x = _x, y = 0;
  do {
    splay(x);
    r[x] = y;
    y = x, x = p[x];
  } while (x);
  return splay(_x);
}
void Link(int x, int y) { splay(x), p[x] = y; }
void Cut(int x) {
  acs(x);
  p[l[x]] = 0, l[x] = 0;
}
int lca(int y, int _x) {
  acs(y);
  int x = _x, z;
  y = 0;
  do {
    splay(x);
    if (!p[x])
      z = x;
    r[x] = y;
    y = x, x = p[x];
  } while (x);
  splay(_x);
  return z;
}
int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  char cmd[9];
  int x, y;
  while (m--) {
    scanf("%s%d", cmd, &x);
    if (cmd[1] != 'u')
      scanf("%d", &y);
    if (cmd[1] == 'i')
      Link(x, y);
    else if (cmd[1] == 'u')
      Cut(x);
    else
      printf("%d\n", lca(x, y));
  }
  return 0;
}
