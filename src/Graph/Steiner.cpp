/* Steiner Problem for POJ 3123
 *
 * Given a graph of n points, and the distance in adjacency matrix.
 * Find the minimum weighted tree containing every node of e.
 * The result is stored in dp[1 << m][n] where d[s][r] means the minimum
 * cost to build a tree from r that connects every point in bitset s.
 * Time Complexity: O(n^3 + 3^m + 2^m * n^2)
 */
void Steiner(int *e, int m) {
  REP(k, n) REP(i, n) REP(j, n) d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
  memset(dp, 0, sizeof dp);
  REP(i, m) REP(j, n) dp[1 << i][j] = d[e[i]][j];
  for (int i = 1; i < (1 << m); ++i) {
    if (!(i & (i - 1))) continue;
    REP(j, n) {
      dp[i][j] = inf;
      for (int k = (i - 1) & i; k; k = (k - 1) & i)
        dp[i][j] = min(dp[i][j], dp[k][j] + dp[i^k][j]);
    }
    memset(v, 0, sizeof v);
    REP(j, n) {
      int c = -1;
      REP(k, n) if (!v[k] && (c == -1 || dp[i][k] < dp[i][c])) c = k;
      v[c] = true;
      REP(k, n) dp[i][c] = min(dp[i][c], dp[i][k] + d[k][c]);
    }
  }
}
