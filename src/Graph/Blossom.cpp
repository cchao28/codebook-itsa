// 一般圖匹配
const int maxn = 550;
struct Blossom {
  int n, match[maxn], pre[maxn], base[maxn];
  vector<int> g[maxn];
  queue<int> q;
  bool inq[maxn], inb[maxn], inp[maxn];
  int lca(int u, int v) {
    fill(inp, inp + n, false);
    for (;; u = pre[match[u]]) {
      u = base[u];
      inp[u] = true;
      if (match[u] == -1) break;
    }
    for (;; v = pre[match[v]]) {
      v = base[v];
      if (inp[v]) return v;
    }
  }
  void reset(int u, int a) {
    while (u != a) {
      int v = match[u];
      inb[base[u]] = inb[base[v]] = true;
      v = pre[v];
      if (base[v] != a) pre[v] = match[u];
      u = v;
    }
  }
  void contract(int u, int v) {
    int a = lca(u, v);
    fill(inb, inb + n, false);
    reset(u, a);
    reset(v, a);
    if (base[u] != a) pre[u] = v;
    if (base[v] != a) pre[v] = u;
    for (int i = 0; i < n; ++i)
      if (inb[base[i]]) {
        base[i] = a;
        if (!inq[i]) {
          q.push(i);
          inq[i] = true;
        }
      }
  }
  bool aug(int s) {
    for (int i = 0; i < n; ++i) {
      pre[i] = -1;
      base[i] = i;
      inq[i] = false;
    }
    q = queue<int>();
    q.push(s);
    while (q.size()) {
      int u = q.front();
      q.pop();
      for (int v : g[u])
        if (base[u] != base[v] && match[u] != v) {
          if (v == s || (match[v] != -1 && pre[match[v]] != -1))
            contract(u, v);
          else if (pre[v] == -1) {
            pre[v] = u;
            if (match[v] != -1) {
              q.push(match[v]);
              inq[match[v]] = true;
            } else {
              u = v;
              while (u != -1) {
                v = pre[u];
                int w = match[v];
                match[u] = v, match[v] = u;
                u = w;
              }
              return true;
            }
          }
        }
    }
    return false;
  }
  void init(int n) {
    this->n = n;
    fill(match, match + n, -1);
    for (int i = 0; i < n; ++i)
      g[i].clear();
  }
  int solve() {
    int ret = 0;
    for (int i = 0; i < n; ++i)
      if (match[i] == -1 && aug(i))
        ret++;
    return ret;
  }
} sol;
